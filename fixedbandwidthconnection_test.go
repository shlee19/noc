package noc

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	akita "gitlab.com/akita/akita/v2"
)

var _ = Describe("FixedBandwidthConnection Integration Test", func() {
	It("Should deliver", func() {
		engine := akita.EngineBuilder{}.Build()
		conn := NewFixedBandwidthConnection(16,
			engine, 1*akita.MHz)
		conn.NumLanes = 1
		agent := newSendRecvAgent("Agent", engine)
		conn.PlugIn(agent.out)
		agent.sendLeft = 50
		agent.TickScheduler.TickLater(0)

		engine.Run()

		Expect(agent.recvCount).To(Equal(agent.sendCount))
	})
})
