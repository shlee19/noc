// Package Flattened provides a Connector and establishes a Flattened connection.
package flattened

import (
	"fmt"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/networking/internal/arbitration"
	"gitlab.com/akita/noc/v2/networking/internal/networking"
	"gitlab.com/akita/noc/v2/networking/internal/routing"
	"gitlab.com/akita/noc/v2/networking/internal/switching"
)

type switchNode struct {
	parentSwitchNode   *switchNode
	currSwitch         *switching.Switch
	localPortToParent  sim.Port
	remotePortToParent sim.Port
}

// Connector can connect devices into a Flattened network.
type FlattenedConnector struct {
	networkName      string
	engine           sim.Engine
	freq             sim.Freq
	encodingOverhead float64
	flitByteSize     int
	switchLatency    int
	switchConnector  *switching.SwitchConnector
	network          *networking.NetworkedConnection
	switchNodes      []*switchNode

	flattenedSwitches []*switching.Switch
	endPoints         []*switching.EndPoint

	config Config

	creditTable *int
	degree      int
	numVC       int
	sizeVC      int
	GPU         bool
}

// NewConnector creates a new connector that can help configure Flattened networks.
func NewConnector() *FlattenedConnector {
	c := &FlattenedConnector{}
	c = c.WithVersion3().
		WithX16().
		WithSwitchLatency(1)
	return c
}

// WithEngine sets the event-driven simulation engine that the Flattened connection
// uses.
func (c *FlattenedConnector) WithEngine(engine sim.Engine) *FlattenedConnector {
	c.engine = engine
	return c
}

// WithVersion3 defines the Flattened connection to use the Flattened-v3 standard.
func (c *FlattenedConnector) WithVersion3() *FlattenedConnector {
	c.freq = 1 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

// WithVersion4 defines the Flattened connection to use the Flattened-v4 standard.
func (c *FlattenedConnector) WithVersion4() *FlattenedConnector {
	c.freq = 2 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

// WithSwitchLatency (shlee)this latency related with each stage in switch
func (c *FlattenedConnector) WithSwitchLatency(numCycles int) *FlattenedConnector {
	c.switchLatency = numCycles
	return c
}

// WithX1 sets Flattened connection to a X1 Flattened connection.
func (c *FlattenedConnector) WithX1() *FlattenedConnector {
	c.flitByteSize = 1
	return c
}

// WithX2 sets Flattened connection to a X1 Flattened connection.
func (c *FlattenedConnector) WithX2() *FlattenedConnector {
	c.flitByteSize = 2
	return c
}

// WithX4 sets Flattened connection to a X1 Flattened connection.
func (c *FlattenedConnector) WithX4() *FlattenedConnector {
	c.flitByteSize = 4
	return c
}

// WithX8 sets Flattened connection to a X1 Flattened connection.
func (c *FlattenedConnector) WithX8() *FlattenedConnector {
	c.flitByteSize = 8
	return c
}

// WithX16 sets Flattened connection to a X1 Flattened connection.
func (c *FlattenedConnector) WithX16() *FlattenedConnector {
	c.flitByteSize = 16
	return c
}

// WithNetworkName sets the name of the network and the prefix of all the
// component in the network.
func (c *FlattenedConnector) WithNetworkName(name string) *FlattenedConnector {
	c.networkName = name
	return c
}

// WithDegree : Width of topology, number of switch per side.
func (c *FlattenedConnector) WithDegree(degree int) *FlattenedConnector {
	c.degree = degree
	return c
}

// WithNumVC : number of VC
func (c *FlattenedConnector) WithNumVC(numVC int) *FlattenedConnector {
	c.numVC = numVC
	return c
}

// WithSizeVC : Legnth of VC buffer
func (c *FlattenedConnector) WithSizeVC(sizeVC int) *FlattenedConnector {
	c.sizeVC = sizeVC
	return c
}

//WithConfig gives the config of router
func (c *FlattenedConnector) WithConfig(config Config) *FlattenedConnector {
	c.config = config
	return c
}

func (c *FlattenedConnector) WithGPU(GPU bool) *FlattenedConnector {
	c.GPU = GPU
	return c
}

func (c *FlattenedConnector) RouterAddress(routerNum int) *switching.Switch {
	return c.flattenedSwitches[routerNum]
}

// CreateNetwork creates a network. This function should be called before
// creating root complexes.
func (c *FlattenedConnector) CreateNetwork() {
	c.network = networking.NewNetworkedConnection()
	c.switchConnector = switching.NewSwitchConnector(c.engine)
	c.switchConnector.SetSwitchLatency(c.switchLatency)
	//Set the latency of stage in router stage.
	//c.switchConnector.SetRouteLatency(c.config.RouteLatency)
	//c.switchConnector.SetvaLatency(c.config.VirtualAllocateLatency)
	//c.switchConnector.SetForwardLatency(c.config.ForwardLatency)
	c.switchConnector.SetRouteLatency(1)
	c.switchConnector.SetvaLatency(1)
	c.switchConnector.SetForwardLatency(1)
}

func (c *FlattenedConnector) routeRootComplexToCPU(
	rc *switching.Switch,
	rcPort sim.Port,
	cpuPorts []sim.Port,
) {
	rt := rc.GetRoutingTable()

	for _, p := range cpuPorts {
		rt.DefineRoute(p, rcPort)
	}

	rt.DefineNumRouters(c.degree * c.degree)
}

// AddSwitch adds a new switch connecting from an existing switch.
func (c *FlattenedConnector) AddFlattenedSwitch() (switchID int) {
	switchID = len(c.flattenedSwitches) + 1
	sw := switching.SwitchBuilder{}.
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithRoutingTable(routing.NewFlattenedTable()).
		WithArbiter(arbitration.NewXBarArbiter()).
		WithDegree(c.degree).
		WithNumVC(c.numVC).
		Build(fmt.Sprintf("%s.Switch%d", c.networkName, switchID))
	c.network.AddSwitch(sw)

	c.flattenedSwitches = append(c.flattenedSwitches, sw)
	c.switchNodes = append(c.switchNodes, &switchNode{
		currSwitch: sw,
	})

	return switchID
}

// ConnectBetweenSwitches is to connect the switch between normal switch
func (c *FlattenedConnector) ConnectBetweenSwitches(Switch1 int, Switch2 int) {
	baseSwitch := c.flattenedSwitches[Switch1-1]
	connectSwitch := c.flattenedSwitches[Switch2-1]

	basePort, newPort := c.switchConnector.
		ConnectSwitches(baseSwitch, connectSwitch, c.freq, c.numVC, c.sizeVC)

	connectSwitch.GetRoutingTable().DefineDefaultRoute(newPort)
	c.switchNodes = append(c.switchNodes, &switchNode{
		parentSwitchNode:   c.switchNodes[Switch1],
		currSwitch:         connectSwitch,
		localPortToParent:  newPort,
		remotePortToParent: basePort,
	})
}

// PlugInDevice connects a series of ports to a switch.
func (c *FlattenedConnector) PlugInFlattened(baseSwitchID int, devicePorts []sim.Port) {
	endPoint := switching.MakeEndPointBuilder().
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithFlitByteSize(c.flitByteSize).
		WithEncodingOverhead(c.encodingOverhead).
		WithDevicePorts(devicePorts).
		WithNumVC(c.numVC).
		WithSizeVC(c.sizeVC).
		Build(fmt.Sprintf("%s.EndPoint%d", c.networkName, len(c.endPoints)))
	c.endPoints = append(c.endPoints, endPoint)
	c.network.AddEndPoint(endPoint)

	baseSwitch := c.flattenedSwitches[baseSwitchID]
	port := c.switchConnector.
		ConnectEndPointToSwitch(endPoint, baseSwitch, c.freq, c.numVC, c.sizeVC)

	sn := c.switchNodes[baseSwitchID]
	c.establishRouteToDevice(sn, port, devicePorts)
}

func (c *FlattenedConnector) establishRouteToDevice(
	sn *switchNode,
	port sim.Port,
	devicePorts []sim.Port,
) {
	if sn == nil {
		return
	}

	rt := sn.currSwitch.GetRoutingTable()
	for _, dst := range devicePorts {
		rt.DefineRoute(dst, port)

		if strings.Contains(dst.Name(), "GPU") {
			sn.currSwitch.GetRoutingTable().DefineConnectGPU(true)
		}
	}

	rt.DefineNumRouters(c.degree * c.degree)
	c.establishRouteToDevice(sn.parentSwitchNode,
		sn.remotePortToParent, devicePorts)
}

func (c *FlattenedConnector) ConnectFlattendButterfly(flattenedConnector *FlattenedConnector, n int) {
	if n < 2 {
		return
	}

	for i := 0; i < n; i++ {
		for j := 1; j < n; j++ {
			for k := 0; k < j; k++ {
				flattenedConnector.ConnectBetweenSwitches(n*i+1+j, n*i+1+k)
			}
		}
	}

	for i := 0; i < n; i++ {
		for j := 1; j < n; j++ {
			for k := 0; k < j; k++ {
				flattenedConnector.ConnectBetweenSwitches(
					(n*(n-2)+1)+i-n*(j-1), (n*(n-1)+1)+i-n*(k))
			}
		}
	}
}
