package switching

import (
	gomock "github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/noc"
)

var _ = Describe("End Point", func() {
	var (
		mockCtrl          *gomock.Controller
		engine            *MockEngine
		devicePort        *MockPort
		networkPort       *MockPort
		defaultSwitchPort *MockPort
		endPoint          *EndPoint

		creditDevicePort               *MockPort
		creditNetworkPort              *MockPort
		creditComplex1, creditComplex2 creditComplex
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		devicePort = NewMockPort(mockCtrl)
		networkPort = NewMockPort(mockCtrl)
		defaultSwitchPort = NewMockPort(mockCtrl)

		creditDevicePort = NewMockPort(mockCtrl)
		creditNetworkPort = NewMockPort(mockCtrl)

		devicePort.EXPECT().SetConnection(gomock.Any())

		endPoint = MakeEndPointBuilder().
			WithEngine(engine).
			WithFreq(1).
			WithFlitByteSize(32).
			WithDevicePorts([]sim.Port{devicePort}).
			Build("end point")
		endPoint.NetworkPort = networkPort
		endPoint.DefaultSwitchDst = defaultSwitchPort
		endPoint.numVC = 1

		credit := make([]int, 1)
		state := make([]string, 1)
		downstreamVCInfo := DownstreamVCInfo{
			credit: credit,
			state:  state,
		}
		downstreamVCInfo.credit[0] = 10
		downstreamVCInfo.state[0] = "IDLE"

		endPoint.creditTableList[networkPort] = downstreamVCInfo
		endPoint.creditTableList[devicePort] = downstreamVCInfo

		creditComplex1 = creditComplex{}
		creditComplex1.localPort = creditDevicePort
		creditComplex2 = creditComplex{}
		creditComplex2.localPort = creditNetworkPort
		endPoint.portToCreditComplexMapping[devicePort] = creditComplex1
		endPoint.portToCreditComplexMapping[networkPort] = creditComplex2
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should send flits", func() {
		msg := &sampleMsg{}
		msg.TrafficBytes = 33

		networkPort.EXPECT().Peek().Return(nil).AnyTimes()

		engine.EXPECT().Schedule(gomock.Any())
		endPoint.Send(msg)

		madeProgress := endPoint.Tick(10)
		Expect(madeProgress).To(BeTrue())

		networkPort.EXPECT().Send(gomock.Any()).Do(func(flit *noc.Flit) {
			Expect(flit.SendTime).To(Equal(sim.VTimeInSec(11)))
			Expect(flit.Src).To(Equal(networkPort))
			Expect(flit.Dst).To(Equal(defaultSwitchPort))
			Expect(flit.SeqID).To(Equal(0))
			Expect(flit.NumFlitInMsg).To(Equal(2))
			Expect(flit.Msg).To(BeIdenticalTo(msg))
		})
		devicePort.EXPECT().NotifyAvailable(gomock.Any())

		madeProgress = endPoint.Tick(11)

		Expect(madeProgress).To(BeTrue())

		networkPort.EXPECT().Send(gomock.Any()).Do(func(flit *noc.Flit) {
			Expect(flit.SendTime).To(Equal(sim.VTimeInSec(12)))
			Expect(flit.Src).To(Equal(networkPort))
			Expect(flit.Dst).To(Equal(defaultSwitchPort))
			Expect(flit.SeqID).To(Equal(1))
			Expect(flit.NumFlitInMsg).To(Equal(2))
			Expect(flit.Msg).To(BeIdenticalTo(msg))
		})

		madeProgress = endPoint.Tick(12)

		Expect(madeProgress).To(BeTrue())

		madeProgress = endPoint.Tick(13)

		Expect(madeProgress).To(BeFalse())
	})

	It("should receive message", func() {
		msg := &sampleMsg{}
		msg.Dst = devicePort

		flit0 := noc.FlitBuilder{}.
			WithSeqID(0).
			WithNumFlitInMsg(2).
			WithMsg(msg).
			Build()
		flit1 := noc.FlitBuilder{}.
			WithSeqID(0).
			WithNumFlitInMsg(2).
			WithMsg(msg).
			Build()

		networkPort.EXPECT().Peek().Return(flit0)
		networkPort.EXPECT().Peek().Return(flit1)
		networkPort.EXPECT().Peek().Return(nil).Times(3)

		networkPort.EXPECT().Retrieve(gomock.Any()).Times(2)

		devicePort.EXPECT().Recv(msg)

		creditNetworkPort.EXPECT().Send(gomock.Any()).AnyTimes()

		madeProgress := endPoint.Tick(10)
		Expect(madeProgress).To(BeTrue())

		madeProgress = endPoint.Tick(11)
		Expect(madeProgress).To(BeTrue())

		madeProgress = endPoint.Tick(12)
		Expect(madeProgress).To(BeTrue())

		madeProgress = endPoint.Tick(13)
		Expect(madeProgress).To(BeTrue())

		madeProgress = endPoint.Tick(14)
		Expect(madeProgress).To(BeFalse())
	})

})
