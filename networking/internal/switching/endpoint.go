package switching

import (
	"fmt"
	"math"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/noc"
)

// EndPoint is an sim component that deligates sending and receiving actions
// of a few ports.
type EndPoint struct {
	*sim.TickingComponent

	DevicePorts      []sim.Port
	NetworkPort      sim.Port
	DefaultSwitchDst sim.Port

	flitByteSize             int
	encodingOverhead         float64
	msgOutBuf                []sim.Msg
	msgOutBufSize            int
	flitsToSend              []*noc.Flit
	flitsToAssemble          []*noc.Flit
	flitAssemblingBufferSize int
	assemblingMsg            sim.Msg
	numFlitReqired           int
	numFlitArrived           int

	//shlee
	creditTableList            map[sim.Port]DownstreamVCInfo
	creditPorts                []sim.Port
	sizeVC                     int
	numVC                      int
	VCToID                     []string
	IDToVC                     map[string]int
	portToCreditComplexMapping map[sim.Port]creditComplex
	lastSelected               int
}

func (ep *EndPoint) addPort(port sim.Port, numVC int, sizeVC int) {
	credit := make([]int, numVC)
	state := make([]string, numVC)
	ct := DownstreamVCInfo{
		credit: credit,
		state:  state,
	}
	ep.creditTableList[port] = ct
	for i := 0; i < numVC; i++ {
		ep.creditTableList[port].credit[i] = sizeVC
		ep.creditTableList[port].state[i] = "IDLE"
	}
}
func (ep *EndPoint) addCreditPort(complex creditComplex, port sim.Port) {
	ep.creditPorts = append(ep.creditPorts, complex.localPort)
	ep.portToCreditComplexMapping[port] = complex
}

// Send sends a message.
func (ep *EndPoint) Send(msg sim.Msg) *sim.SendError {
	ep.Lock()
	defer ep.Unlock()

	if len(ep.msgOutBuf) >= ep.msgOutBufSize {
		return &sim.SendError{}
	}
	ep.msgOutBuf = append(ep.msgOutBuf, msg)
	ep.TickLater(msg.Meta().SendTime)

	return nil
}

// PlugIn connects a port to the endpoint.
func (ep *EndPoint) PlugIn(port sim.Port, srcBufCap int) {
	port.SetConnection(ep)
	ep.DevicePorts = append(ep.DevicePorts, port)
	ep.msgOutBufSize = srcBufCap
}

// NotifyAvailable triggers the endpoint to continue to tick.
func (ep *EndPoint) NotifyAvailable(now sim.VTimeInSec, port sim.Port) {
	ep.TickLater(now)
}

// Unplug removes the association of a port and an endpoint.
func (ep *EndPoint) Unplug(port sim.Port) {
	panic("not implemented")
}

// Tick update the endpoint state.
func (ep *EndPoint) Tick(now sim.VTimeInSec) bool {
	ep.Lock()
	defer ep.Unlock()

	madeProgress := false
	madeProgress = ep.sendFlitOut(now) || madeProgress
	madeProgress = ep.prepareFlits(now) || madeProgress
	madeProgress = ep.tryDeliver(now) || madeProgress
	madeProgress = ep.assemble(now) || madeProgress
	madeProgress = ep.recv(now) || madeProgress

	madeProgress = ep.creditCheck(now) || madeProgress

	return madeProgress
}

// creditCheck check credit arrives on endpoint
func (ep *EndPoint) creditCheck(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range ep.creditPorts {
		item := port.Peek()
		if item == nil {
			continue
		}
		credit := item.(*noc.Flit)
		VCnum := credit.UpstreamVCnum
		creditTable := ep.creditTableList[credit.UpstreamPort]
		creditTable.credit[VCnum] = creditTable.credit[VCnum] + 1

		port.Retrieve(now)
		madeProgress = true
	}
	return madeProgress
}

// sendFlitOut allocate the VC of downstream Router by flit type.
func (ep *EndPoint) sendFlitOut(now sim.VTimeInSec) bool {
	if len(ep.flitsToSend) == 0 {
		return false
	}
	ep.flitsToSend[0].SendTime = now
	downstreamVCInfo := ep.creditTableList[ep.NetworkPort]

	ID := ep.flitsToSend[0].Msg.Meta().ID
	if ep.flitsToSend[0].FlitType == "HEAD" {
		VC := ep.assignVCInEndPoint(ep.flitsToSend[0])

		if VC == -1 || downstreamVCInfo.credit[VC] <= 0 {
			return false
		}

		downstreamVCInfo.state[VC] = "ACTIVE"
		ep.IDToVC[ID] = VC
	} else if ep.flitsToSend[0].FlitType == "TAIL" {
		VC := ep.IDToVC[ID]

		if downstreamVCInfo.credit[VC] <= 0 {
			return false
		}

		downstreamVCInfo.state[VC] = "IDLE"
	} else if ep.flitsToSend[0].FlitType == "SIGNAL" {
		VC := ep.assignVCInEndPoint(ep.flitsToSend[0])

		if VC == -1 || downstreamVCInfo.credit[VC] <= 0 {
			return false
		}
	}

	VC := ep.IDToVC[ID]
	if downstreamVCInfo.credit[VC] <= 0 {
		return false
	}

	downstreamVCInfo.credit[VC] = downstreamVCInfo.credit[VC] - 1
	ep.flitsToSend[0].VCnum = VC

	err := ep.NetworkPort.Send(ep.flitsToSend[0])

	if err == nil {
		ep.flitsToSend = ep.flitsToSend[1:]
		if len(ep.flitsToSend) == 0 {
			for _, p := range ep.DevicePorts {
				p.NotifyAvailable(now)
			}
		}
		return true
	}

	return false
}

func (ep *EndPoint) assignVCInEndPoint(f *noc.Flit) int {
	downstreamVCInfo := ep.creditTableList[ep.NetworkPort]
	for i := 0; i < ep.numVC; i++ {
		bufIndex := (i + ep.lastSelected + 1) % ep.numVC
		if downstreamVCInfo.state[bufIndex] == "IDLE" {
			ep.lastSelected = bufIndex
			return bufIndex
		}
	}

	return -1
}

func (ep *EndPoint) prepareFlits(now sim.VTimeInSec) bool {
	if len(ep.flitsToSend) > 0 {
		return false
	}
	if len(ep.msgOutBuf) == 0 {
		return false
	}
	msg := ep.msgOutBuf[0]
	ep.msgOutBuf = ep.msgOutBuf[1:]
	ep.flitsToSend = ep.msgToFlits(msg)
	return true
}

func (ep *EndPoint) recv(now sim.VTimeInSec) bool {
	recved := ep.NetworkPort.Peek()
	if recved == nil {
		return false
	}
	flit := recved.(*noc.Flit)
	creditComplex := ep.portToCreditComplexMapping[ep.NetworkPort]
	src := creditComplex.localPort
	dst := creditComplex.remotePort
	credit := noc.FlitBuilder{}.
		WithSrc(src).
		WithDst(dst).
		WithSendTime(now).
		Build()
	credit.UpstreamPort = flit.Src
	credit.UpstreamVCnum = flit.VCnum
	err := src.Send(credit)
	if err != nil { // See if credit was sent to upstream router.
		return false
	}

	ep.flitsToAssemble = append(ep.flitsToAssemble, flit)
	if len(ep.flitsToAssemble) >= ep.flitAssemblingBufferSize {
		//log.Printf("warning: flit buffer overflow, " +
		//	"double buffer size to prevent deadlock")
		ep.flitAssemblingBufferSize *= 2
	}
	ep.NetworkPort.Retrieve(now)
	//log.Printf("%.12f, EP %s, received flit %d(%d), msg %s\n",
	//	now, ep.Name(), flit.SeqID, flit.NumFlitInMsg, flit.Msg.Meta().ID)

	return true
}

func (ep *EndPoint) assemble(now sim.VTimeInSec) bool {
	madeProgress := false
	newFlits := make([]*noc.Flit, 0)
	for _, f := range ep.flitsToAssemble {
		if ep.assemblingMsg == nil {
			ep.assemblingMsg = f.Msg
			ep.numFlitArrived = 1
			ep.numFlitReqired = f.NumFlitInMsg
			madeProgress = true
		} else if ep.assemblingMsg != nil && f.Msg == ep.assemblingMsg {
			ep.numFlitArrived++
			madeProgress = true
		} else {
			newFlits = append(newFlits, f)
		}
	}

	ep.flitsToAssemble = newFlits
	return madeProgress
}

func (ep *EndPoint) tryDeliver(now sim.VTimeInSec) bool {
	if ep.assemblingMsg == nil {
		return false
	}

	if ep.numFlitArrived < ep.numFlitReqired {
		return false
	}

	ep.assemblingMsg.Meta().RecvTime = now
	err := ep.assemblingMsg.Meta().Dst.Recv(ep.assemblingMsg)
	if err == nil {
		// log.Printf("%.12f, EP %s, msg %s assembled and deliverd\n",
		// 	now, ep.Name(), ep.assemblingMsg.Meta().ID)
		ep.assemblingMsg = nil
		ep.numFlitReqired = 0
		ep.numFlitArrived = 0
		return true
	}

	return false
}

func (ep *EndPoint) msgToFlits(msg sim.Msg) []*noc.Flit {
	numFlit := 1
	if msg.Meta().TrafficBytes > 0 {
		trafficByte := msg.Meta().TrafficBytes
		trafficByte += int(math.Ceil(
			float64(trafficByte) * ep.encodingOverhead))
		numFlit = (trafficByte-1)/ep.flitByteSize + 1
	}

	flits := make([]*noc.Flit, numFlit)
	for i := 0; i < numFlit; i++ {
		flits[i] = noc.FlitBuilder{}.
			WithSrc(ep.NetworkPort).
			WithDst(ep.DefaultSwitchDst).
			WithSeqID(i).
			WithNumFlitInMsg(numFlit).
			WithMsg(msg).
			Build()
		if i == 0 {
			flits[i].FlitType = "HEAD"
		} else if i == numFlit-1 {
			flits[i].FlitType = "TAIL"
		} else {
			flits[i].FlitType = "BODY"
		}
	}

	if numFlit == 1 {
		flits[0].FlitType = "SIGNAL"
	}

	return flits
}

// EndPointBuilder can build End Points.
type EndPointBuilder struct {
	engine                   sim.Engine
	freq                     sim.Freq
	flitByteSize             int
	encodingOverhead         float64
	flitAssemblingBufferSize int
	networkPortBufferSize    int
	devicePorts              []sim.Port
	numVC                    int
	sizeVC                   int
}

// MakeEndPointBuilder creates a new EndPointBuilder with default
// configureations.
func MakeEndPointBuilder() EndPointBuilder {
	return EndPointBuilder{
		flitByteSize:             32,
		flitAssemblingBufferSize: 64,
		networkPortBufferSize:    4,
		freq:                     1 * sim.GHz,
	}
}

// WithEngine sets the engine of the End Point to build.
func (b EndPointBuilder) WithEngine(e sim.Engine) EndPointBuilder {
	b.engine = e
	return b
}

// WithFreq sets the frequency of the End Point to built.
func (b EndPointBuilder) WithFreq(freq sim.Freq) EndPointBuilder {
	b.freq = freq
	return b
}

// WithFlitByteSize sets the flit byte size that the End Point supports.
func (b EndPointBuilder) WithFlitByteSize(n int) EndPointBuilder {
	b.flitByteSize = n
	return b
}

// WithEncodingOverhead sets the encoding overhead.
func (b EndPointBuilder) WithEncodingOverhead(o float64) EndPointBuilder {
	b.encodingOverhead = o
	return b
}

// WithNetworkPortBufferSize sets the network port buffer size of the end point.
func (b EndPointBuilder) WithNetworkPortBufferSize(n int) EndPointBuilder {
	b.networkPortBufferSize = n
	return b
}

// WithDevicePorts sets a list of ports that communicate directly through the
// End Point.
func (b EndPointBuilder) WithDevicePorts(ports []sim.Port) EndPointBuilder {
	b.devicePorts = ports
	return b
}

//WithNumVC set the the VC
func (b EndPointBuilder) WithNumVC(numVC int) EndPointBuilder {
	b.numVC = numVC
	return b
}

//WithSizeVC set the size of VC
func (b EndPointBuilder) WithSizeVC(sizeVC int) EndPointBuilder {
	b.sizeVC = sizeVC
	return b
}

// Build creates a new End Point.
func (b EndPointBuilder) Build(name string) *EndPoint {
	b.engineMustBeGiven()
	b.freqMustBeGiven()
	b.flitByteSizeMustBeGiven()

	ep := &EndPoint{}
	ep.TickingComponent = sim.NewTickingComponent(
		name, b.engine, b.freq, ep)
	ep.flitByteSize = b.flitByteSize
	ep.flitAssemblingBufferSize = 64
	ep.NetworkPort = sim.NewLimitNumMsgPort(
		ep, b.networkPortBufferSize,
		fmt.Sprintf("%s.network_port", ep.Name()))

	ep.numVC = b.numVC
	ep.sizeVC = b.sizeVC
	ep.creditTableList = make(map[sim.Port]DownstreamVCInfo)
	ep.VCToID = make([]string, b.numVC)
	ep.IDToVC = make(map[string]int)
	ep.portToCreditComplexMapping = make(map[sim.Port]creditComplex)

	for _, dp := range b.devicePorts {
		ep.PlugIn(dp, 1)
	}

	return ep
}

func (b EndPointBuilder) engineMustBeGiven() {
	if b.engine == nil {
		panic("engine is not given")
	}
}

func (b EndPointBuilder) freqMustBeGiven() {
	if b.freq == 0 {
		panic("freq must be given")
	}
}

func (b EndPointBuilder) flitByteSizeMustBeGiven() {
	if b.flitByteSize == 0 {
		panic("flit byte size must be given")
	}
}

func TranslateGPUPortNumber(port sim.Port) int {
	// 100 can available until 10*10 topology
	for i := 10; i >= 0; i-- {
		if strings.Contains(port.Name(), fmt.Sprintf("GPU%d", i)) {
			return i
		}
	}

	return -1
}
