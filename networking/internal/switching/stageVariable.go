package switching

func NewStageVariable(
	setRoute int,
	setVirtualAllocate int,
	setForward int,
) *stageVariable {
	sv := &stageVariable{}

	sv.tempRoute = 0
	sv.tempVirtualAllocate = 0
	sv.tempForward = 0

	sv.setRoute = setRoute
	sv.setVirtualAllocate = setVirtualAllocate
	sv.setForward = setForward

	return sv
}

type stageVariable struct {
	setRoute           int
	setVirtualAllocate int
	setForward         int

	tempRoute           int
	tempVirtualAllocate int
	tempForward         int
}

//CheckRoute is function to control the cycle of route stage
func (sv *stageVariable) CheckRoute() int {
	sv.tempRoute++
	if sv.tempRoute == sv.setRoute {
		sv.tempRoute = 0
		return 1
	} else {
		return 0
	}
}

//CheckVirtualAllocate is function to control the cycle of route stage
func (sv *stageVariable) CheckVirtualAllocate() int {
	sv.tempVirtualAllocate = sv.tempVirtualAllocate + 1

	if sv.tempVirtualAllocate == sv.setVirtualAllocate {
		sv.tempVirtualAllocate = 0
		return 1
	} else {
		return 0
	}
}

//CheckForward is function to control the cycle of route stage
func (sv *stageVariable) CheckForward() int {
	sv.tempForward++
	if sv.tempForward == sv.setForward {
		sv.tempForward = 0
		return 1
	} else {
		return 0
	}
}
