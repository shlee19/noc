package switching

import (
	gomock "github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc"
	"gitlab.com/akita/util"
)

func createMockPortComplex(ctrl *gomock.Controller) portComplex {
	local := NewMockPort(ctrl)
	remote := NewMockPort(ctrl)
	routeBuf := NewMockBuffer(ctrl)
	virtualBuf := NewMockBuffer(ctrl)
	forwardBuf := NewMockBuffer(ctrl)
	sendOutBuf := NewMockBuffer(ctrl)
	pipeline := NewMockPipeline(ctrl)

	latencyVar := NewStageVariable(1, 1, 1)

	pc := portComplex{
		localPort:       local,
		remotePort:      remote,
		pipeline:        pipeline,
		routeBuffer:     routeBuf,
		virtualBuffer:   virtualBuf,
		forwardBuffer:   forwardBuf,
		sendOutBuffer:   sendOutBuf,
		sizeVC:          10,
		latencyVariable: latencyVar,
	}

	return pc
}

var _ = Describe("Switch", func() {
	var (
		mockCtrl                   *gomock.Controller
		engine                     *MockEngine
		portComplex1, portComplex2 portComplex
		dstPort                    *MockPort
		routingTable               *MockTable
		arbiter                    *MockArbiter
		sw                         *Switch

		creditPort1, creditPort2                     *MockPort
		VCs1, VCs2                                   []portComplex
		vcList1, vcList2                             VCList
		outComplex1, outComplex2                     outComplex
		downstreamVCInfoList1, downstreamVCInfoList2 DownstreamVCInfo
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		portComplex1 = createMockPortComplex(mockCtrl)
		portComplex2 = createMockPortComplex(mockCtrl)
		dstPort = NewMockPort(mockCtrl)
		dstPort.EXPECT().Name().AnyTimes()

		creditPort1 = NewMockPort(mockCtrl)
		creditPort2 = NewMockPort(mockCtrl)

		routingTable = NewMockTable(mockCtrl)
		arbiter = NewMockArbiter(mockCtrl)
		arbiter.EXPECT().AddBuffer(gomock.Any()).AnyTimes()
		sw = SwitchBuilder{}.
			WithEngine(engine).
			WithFreq(1).
			WithRoutingTable(routingTable).
			WithArbiter(arbiter).
			WithNumVC(1).
			Build("switch")
		///////////////////////////////////////////////////////////////////////

		VCs1 = make([]portComplex, 1)
		VCs2 = make([]portComplex, 1)
		vcList1 = VCList{
			VC:        VCs1,
			localPort: portComplex1.localPort,
		}
		vcList2 = VCList{
			VC:        VCs2,
			localPort: portComplex2.localPort,
		}
		vcList1.VC[0] = portComplex1
		vcList2.VC[0] = portComplex2
		out1 := NewMockBuffer(mockCtrl)
		out2 := NewMockBuffer(mockCtrl)
		outComplex1 = outComplex{
			outBuf:     out1,
			localPort:  portComplex1.localPort,
			remotePort: portComplex2.remotePort,
		}
		outComplex2 = outComplex{
			outBuf:     out2,
			localPort:  portComplex2.localPort,
			remotePort: portComplex2.remotePort,
		}

		sw.portToOutComplex[dstPort] = outComplex2

		sw.portToVCList[portComplex1.localPort] = vcList1
		sw.portToVCList[portComplex2.localPort] = vcList2

		credit := make([]int, 1)
		state := make([]string, 1)
		downstreamVCInfoList1 = DownstreamVCInfo{
			credit: credit,
			state:  state,
		}
		downstreamVCInfoList1.credit[0] = 10
		downstreamVCInfoList1.state[0] = "IDLE"
		downstreamVCInfoList2 = DownstreamVCInfo{
			credit: credit,
			state:  state,
		}
		downstreamVCInfoList2.credit[0] = 10
		downstreamVCInfoList2.state[0] = "IDLE"
		sw.downstreamVCInfoList[portComplex1.localPort] = downstreamVCInfoList1
		sw.downstreamVCInfoList[portComplex2.localPort] = downstreamVCInfoList2

		creditComplex1 := creditComplex{
			localPort: creditPort1,
		}
		creditComplex2 := creditComplex{
			localPort: creditPort2,
		}
		sw.portToCreditComplexMapping[portComplex1.localPort] = creditComplex1
		sw.portToCreditComplexMapping[portComplex2.localPort] = creditComplex2

		sw.addPort(vcList1, outComplex1, 1, 10)
		sw.addPort(vcList2, outComplex2, 1, 10)

	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should start processing", func() {
		sw.forPortMapping = 1
		port1 := portComplex1.localPort.(*MockPort)
		port2 := portComplex2.localPort.(*MockPort)

		port1Pipeline := portComplex1.pipeline.(*MockPipeline)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithDst(port1).
			WithMsg(msg).
			Build()
		flit.VCnum = 0
		port1.EXPECT().Peek().Return(flit)
		port1.EXPECT().Retrieve(gomock.Any())
		port2.EXPECT().Peek().Return(nil)
		port1Pipeline.EXPECT().CanAccept().Return(true)
		port1Pipeline.
			EXPECT().
			Accept(akita.VTimeInSec(10), gomock.Any()).
			Do(func(now akita.VTimeInSec, i flitPipelineItem) {
				Expect(i.flit).To(Equal(flit))
			})
		madeProgress := sw.startProcessing(10)
		Expect(madeProgress).To(BeTrue())
	})

	It("should not start processing if pipeline is busy", func() {
		sw.forPortMapping = 1
		port1 := portComplex1.localPort.(*MockPort)
		port2 := portComplex2.localPort.(*MockPort)
		port1Pipeline := portComplex1.pipeline.(*MockPipeline)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithDst(port1).
			WithMsg(msg).
			Build()

		port1.EXPECT().Peek().Return(flit)
		port2.EXPECT().Peek().Return(nil)
		port1Pipeline.EXPECT().CanAccept().Return(false)

		madeProgress := sw.startProcessing(10)

		Expect(madeProgress).To(BeFalse())
	})

	It("should tick the pipelines", func() {
		port1Pipeline := portComplex1.pipeline.(*MockPipeline)
		port2Pipeline := portComplex2.pipeline.(*MockPipeline)

		port1Pipeline.EXPECT().Tick(akita.VTimeInSec(10)).Return(false)
		port2Pipeline.EXPECT().Tick(akita.VTimeInSec(10)).Return(true)

		madeProgress := sw.movePipeline(10)

		Expect(madeProgress).To(BeTrue())
	})

	It("should route", func() {
		routeBuffer1 := portComplex1.routeBuffer.(*MockBuffer)
		routeBuffer2 := portComplex2.routeBuffer.(*MockBuffer)
		virtualBuffer1 := portComplex1.virtualBuffer.(*MockBuffer)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()

		pipelineItem := flitPipelineItem{taskID: "flit", flit: flit}
		routeBuffer1.EXPECT().Peek().Return(pipelineItem)
		routeBuffer1.EXPECT().Pop()
		routeBuffer2.EXPECT().Peek().Return(nil)
		virtualBuffer1.EXPECT().CanPush().Return(true)
		virtualBuffer1.EXPECT().Push(flit)
		routingTable.EXPECT().FindPort(gomock.Any(), gomock.Any(), dstPort, dstPort, 0).Return(portComplex2.localPort)

		madeProgress := sw.route(10)

		Expect(madeProgress).To(BeTrue())
		Expect(flit.OutputBuf).To(BeIdenticalTo(outComplex2.outBuf))
	})

	It("should not route if forward buffer is full", func() {
		routeBuffer1 := portComplex1.routeBuffer.(*MockBuffer)
		routeBuffer2 := portComplex2.routeBuffer.(*MockBuffer)
		forwardBuffer1 := portComplex1.virtualBuffer.(*MockBuffer)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()

		pipelineItem := flitPipelineItem{taskID: "flit", flit: flit}
		routeBuffer1.EXPECT().Peek().Return(pipelineItem)
		routeBuffer2.EXPECT().Peek().Return(nil)
		forwardBuffer1.EXPECT().CanPush().Return(false)

		madeProgress := sw.route(10)

		Expect(madeProgress).To(BeFalse())
	})

	It("should forward", func() {
		forwardBuffer1 := portComplex1.forwardBuffer.(*MockBuffer)
		forwardBuffer2 := portComplex2.forwardBuffer.(*MockBuffer)

		sendOutBuffer2 := outComplex2.outBuf.(*MockBuffer)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()
		flit.OutputPort = portComplex2.localPort
		flit.OutputBuf = outComplex2.outBuf
		flit.Dst = portComplex1.localPort
		flit.VCnum = 0
		flit.FlitType = "HEAD"

		forwardBuffer1.EXPECT().Peek().Return(flit)
		forwardBuffer1.EXPECT().Pop()
		forwardBuffer2.EXPECT().Peek().Return(nil)

		sendOutBuffer2.EXPECT().CanPush().Return(true)
		sendOutBuffer2.EXPECT().Push(flit)

		arbiter.EXPECT().UpdateDownStreamVCInfo(flit, 10)

		arbiter.EXPECT().
			Arbitrate(akita.VTimeInSec(10)).
			Return([]util.Buffer{forwardBuffer1, forwardBuffer2})

		creditPort1.EXPECT().Send(gomock.Any()).AnyTimes()

		forwardBuffer1.EXPECT().Peek().Return(flit)
		forwardBuffer2.EXPECT().Peek().Return(nil)

		madeProgress := sw.forward(10)

		Expect(madeProgress).To(BeTrue())
		Expect(flit.VCnum).To(BeIdenticalTo(0))
	})

	It("should not forward if the output buffer is busy", func() {
		forwardBuffer1 := portComplex1.forwardBuffer.(*MockBuffer)
		forwardBuffer2 := portComplex2.forwardBuffer.(*MockBuffer)

		sendOutBuffer2 := outComplex2.outBuf.(*MockBuffer)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()
		flit.OutputPort = portComplex2.localPort
		flit.OutputBuf = outComplex2.outBuf
		flit.Dst = portComplex1.localPort
		flit.VCnum = 0
		flit.FlitType = "HEAD"

		forwardBuffer1.EXPECT().Peek().Return(flit)
		forwardBuffer2.EXPECT().Peek().Return(nil)
		arbiter.EXPECT().UpdateDownStreamVCInfo(flit, 10)

		arbiter.EXPECT().
			Arbitrate(akita.VTimeInSec(10)).
			Return([]util.Buffer{forwardBuffer1, forwardBuffer2})
		forwardBuffer1.EXPECT().Peek().Return(flit)
		forwardBuffer2.EXPECT().Peek().Return(nil)

		sendOutBuffer2.EXPECT().CanPush().Return(false)
		madeProgress := sw.forward(10)

		Expect(madeProgress).To(BeFalse())
	})

	It("should send flits out", func() {
		sendOutBuffer1 := outComplex1.outBuf.(*MockBuffer)
		sendOutBuffer2 := outComplex2.outBuf.(*MockBuffer)

		localPort2 := outComplex2.localPort.(*MockPort)
		remotePort2 := outComplex2.remotePort.(*MockPort)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()

		sendOutBuffer1.EXPECT().Peek().Return(nil)
		sendOutBuffer2.EXPECT().Peek().Return(flit)
		sendOutBuffer2.EXPECT().Pop()
		localPort2.EXPECT().Send(flit).Return(nil)

		madeProgress := sw.sendOut(10)

		Expect(madeProgress).To(BeTrue())
		Expect(flit.Dst).To(BeIdenticalTo(remotePort2))
		Expect(flit.Src).To(BeIdenticalTo(outComplex2.localPort))
		Expect(flit.SendTime).To(Equal(akita.VTimeInSec(10)))
	})

	It("should wait ifport is busy flits out", func() {
		sendOutBuffer1 := outComplex1.outBuf.(*MockBuffer)
		sendOutBuffer2 := outComplex2.outBuf.(*MockBuffer)

		localPort2 := outComplex2.localPort.(*MockPort)
		remotePort2 := outComplex2.remotePort.(*MockPort)

		msg := &sampleMsg{}
		msg.Src = dstPort
		msg.Dst = dstPort
		flit := noc.FlitBuilder{}.
			WithMsg(msg).
			Build()

		sendOutBuffer1.EXPECT().Peek().Return(nil)
		sendOutBuffer2.EXPECT().Peek().Return(flit)
		localPort2.EXPECT().Send(flit).Return(&akita.SendError{})

		madeProgress := sw.sendOut(10)

		Expect(madeProgress).To(BeFalse())
		Expect(flit.Dst).To(BeIdenticalTo(remotePort2))
		Expect(flit.Src).To(BeIdenticalTo(portComplex2.localPort))
		Expect(flit.SendTime).To(Equal(akita.VTimeInSec(10)))

	})

})
