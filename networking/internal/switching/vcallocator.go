package switching

import (
	"strings"
	//"fmt"
	"gitlab.com/akita/akita/v2/sim"
)

type vcAllocator struct {
	outPortToVCmapping map[sim.Port]int
	lastSelected       int
}

func (a *vcAllocator) allocate(
	outputPort sim.Port,
	flitType string,
	downstreamVCInfoList map[sim.Port]DownstreamVCInfo,
	numVC int,
) int {
	var downstreamVC int
	if strings.Contains(flitType, "HEAD") || strings.Contains(flitType, "SIGNAL") {
		downstreamVC = a.findVC(downstreamVCInfoList[outputPort], numVC)
		if downstreamVC < 0 {
			return -1
		}
		a.outPortToVCmapping[outputPort] = downstreamVC
		downstreamVCInfoList[outputPort].state[downstreamVC] = "WAITING"
	} else {
		downstreamVC = a.outPortToVCmapping[outputPort]
	}

	return downstreamVC
}

// findVC allocates VCs in a round-robin manner.
func (a *vcAllocator) findVC(info DownstreamVCInfo, numVC int) int {
	for i := 0; i < numVC; i++ {
		bufIndex := (i + a.lastSelected + 1) % numVC
		if info.state[bufIndex] == "IDLE" {
			a.lastSelected = bufIndex
			return bufIndex
		}
	}

	return -1
}
