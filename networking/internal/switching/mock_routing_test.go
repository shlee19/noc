// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/akita/noc/networking/internal/routing (interfaces: Table)

package switching

import (
	gomock "github.com/golang/mock/gomock"
	akita "gitlab.com/akita/akita"
	reflect "reflect"
)

// MockTable is a mock of Table interface
type MockTable struct {
	ctrl     *gomock.Controller
	recorder *MockTableMockRecorder
}

// MockTableMockRecorder is the mock recorder for MockTable
type MockTableMockRecorder struct {
	mock *MockTable
}

// NewMockTable creates a new mock instance
func NewMockTable(ctrl *gomock.Controller) *MockTable {
	mock := &MockTable{ctrl: ctrl}
	mock.recorder = &MockTableMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockTable) EXPECT() *MockTableMockRecorder {
	return m.recorder
}

// DefineConnectGPU mocks base method
func (m *MockTable) DefineConnectGPU(arg0 bool) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "DefineConnectGPU", arg0)
}

// DefineConnectGPU indicates an expected call of DefineConnectGPU
func (mr *MockTableMockRecorder) DefineConnectGPU(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DefineConnectGPU", reflect.TypeOf((*MockTable)(nil).DefineConnectGPU), arg0)
}

// DefineDefaultRoute mocks base method
func (m *MockTable) DefineDefaultRoute(arg0 akita.Port) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "DefineDefaultRoute", arg0)
}

// DefineDefaultRoute indicates an expected call of DefineDefaultRoute
func (mr *MockTableMockRecorder) DefineDefaultRoute(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DefineDefaultRoute", reflect.TypeOf((*MockTable)(nil).DefineDefaultRoute), arg0)
}

// DefineNumRouters mocks base method
func (m *MockTable) DefineNumRouters(arg0 int) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "DefineNumRouters", arg0)
}

// DefineNumRouters indicates an expected call of DefineNumRouters
func (mr *MockTableMockRecorder) DefineNumRouters(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DefineNumRouters", reflect.TypeOf((*MockTable)(nil).DefineNumRouters), arg0)
}

// DefineRoute mocks base method
func (m *MockTable) DefineRoute(arg0, arg1 akita.Port) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "DefineRoute", arg0, arg1)
}

// DefineRoute indicates an expected call of DefineRoute
func (mr *MockTableMockRecorder) DefineRoute(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DefineRoute", reflect.TypeOf((*MockTable)(nil).DefineRoute), arg0, arg1)
}

// FindPort mocks base method
func (m *MockTable) FindPort(arg0 string, arg1 map[int]akita.Port, arg2, arg3 akita.Port, arg4 int) akita.Port {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FindPort", arg0, arg1, arg2, arg3, arg4)
	ret0, _ := ret[0].(akita.Port)
	return ret0
}

// FindPort indicates an expected call of FindPort
func (mr *MockTableMockRecorder) FindPort(arg0, arg1, arg2, arg3, arg4 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FindPort", reflect.TypeOf((*MockTable)(nil).FindPort), arg0, arg1, arg2, arg3, arg4)
}

// TranslateAgentPortNumber mocks base method
func (m *MockTable) TranslateAgentPortNumber(arg0 akita.Port) int {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "TranslateAgentPortNumber", arg0)
	ret0, _ := ret[0].(int)
	return ret0
}

// TranslateAgentPortNumber indicates an expected call of TranslateAgentPortNumber
func (mr *MockTableMockRecorder) TranslateAgentPortNumber(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "TranslateAgentPortNumber", reflect.TypeOf((*MockTable)(nil).TranslateAgentPortNumber), arg0)
}

// TranslateNumber mocks base method
func (m *MockTable) TranslateNumber(arg0 string) int {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "TranslateNumber", arg0)
	ret0, _ := ret[0].(int)
	return ret0
}

// TranslateNumber indicates an expected call of TranslateNumber
func (mr *MockTableMockRecorder) TranslateNumber(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "TranslateNumber", reflect.TypeOf((*MockTable)(nil).TranslateNumber), arg0)
}
