package switching

import (
	"fmt"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/util/v2/buffering"
	"gitlab.com/akita/util/v2/pipelining"
)

// SwitchConnector can connect switches and ports together.
type SwitchConnector struct {
	engine              sim.Engine
	bufferSizeInNumFlit int
	switchLatency       int
	routeLatency        int
	vaLatency           int
	forwardLatency      int
}

// NewSwitchConnector creates a new switch connector.
func NewSwitchConnector(engine sim.Engine) *SwitchConnector {
	c := &SwitchConnector{
		engine:              engine,
		bufferSizeInNumFlit: 1,
		switchLatency:       1,
	}
	return c
}

// SetBufferSize sets the default buffer size of the port at each switching for
// incoming messages.
func (c *SwitchConnector) SetBufferSize(numFlit int) {
	c.bufferSizeInNumFlit = numFlit
}

// SetSwitchLatency sets the number of cycles required between when the message
// arrives at a switch and when the message is forwarded.
func (c *SwitchConnector) SetSwitchLatency(numCycles int) {
	c.switchLatency = numCycles
}

func (c *SwitchConnector) SetRouteLatency(latency int) {
	c.routeLatency = latency
}

func (c *SwitchConnector) SetvaLatency(latency int) {
	c.vaLatency = latency
}

func (c *SwitchConnector) SetForwardLatency(latency int) {
	c.forwardLatency = latency
}

// ConnectSwitches connect two switches together.
func (c *SwitchConnector) ConnectSwitches(
	a, b *Switch,
	freq sim.Freq,
	numVC int,
	sizeVC int,
) (portOnA, portOnB sim.Port) {
	portA := sim.NewLimitNumMsgPort(a, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.Port%d", a.Name(), len(a.ports)))
	portB := sim.NewLimitNumMsgPort(b, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.Port%d", b.Name(), len(b.ports)))
	conn := sim.NewDirectConnection(
		fmt.Sprintf("%s-%s", portA.Name(), portB.Name()),
		c.engine, freq)
	conn.PlugIn(portA, 1)
	conn.PlugIn(portB, 1)
	a.addPort(c.createVCList(portA, portB, numVC, sizeVC),
		c.createOutComplex(portA, portB), numVC, sizeVC)
	b.addPort(c.createVCList(portB, portA, numVC, sizeVC),
		c.createOutComplex(portB, portA), numVC, sizeVC)

	creditPortA := sim.NewLimitNumMsgPort(a, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.CreditPort%d", a.Name(), len(a.creditPorts)))
	creditPortB := sim.NewLimitNumMsgPort(b, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.CreditPort%d", a.Name(), len(b.creditPorts)))
	creditConn := sim.NewDirectConnection(
		fmt.Sprintf("Credit.%s-%s", creditPortA.Name(), creditPortB.Name()),
		c.engine, freq)
	creditConn.PlugIn(creditPortA, 1)
	creditConn.PlugIn(creditPortB, 1)

	a.addCreditPort(c.createCreditComplex(creditPortA, creditPortB), portA)
	b.addCreditPort(c.createCreditComplex(creditPortB, creditPortA), portB)

	return portA, portB
}

func (c *SwitchConnector) createPortComplex(
	local, remote sim.Port, numVC int,
) portComplex {
	sendOutBuf := buffering.NewBuffer(1)
	forwardBuf := buffering.NewBuffer(1)
	routeBuf := buffering.NewBuffer(1)
	pipeline := pipelining.NewPipeline(
		local.Name()+"pipeline", c.switchLatency, 1, routeBuf)

	pc := portComplex{
		localPort:     local,
		remotePort:    remote,
		pipeline:      pipeline,
		routeBuffer:   routeBuf,
		forwardBuffer: forwardBuf,
		sendOutBuffer: sendOutBuf,
	}

	return pc
}

func (c *SwitchConnector) createVCList(
	local, remote sim.Port, numVC int, sizeVC int,
) VCList {
	vcs := make([]portComplex, numVC)

	for i := 0; i < numVC; i++ {
		sendOutBuf := buffering.NewBuffer(1)
		forwardBuf := buffering.NewBuffer(1)
		virtualBuf := buffering.NewBuffer(1)
		routeBuf := buffering.NewBuffer(sizeVC - 2)
		pipeline := pipelining.NewPipeline(
			local.Name()+".pipeline", c.switchLatency, 1, routeBuf)

		a := &vcAllocator{}
		a.outPortToVCmapping = make(map[sim.Port]int)

		sv := NewStageVariable(c.routeLatency, c.vaLatency, c.forwardLatency)

		pc := portComplex{
			localPort:       local,
			remotePort:      remote,
			pipeline:        pipeline,
			routeBuffer:     routeBuf,
			virtualBuffer:   virtualBuf,
			forwardBuffer:   forwardBuf,
			sendOutBuffer:   sendOutBuf,
			sizeVC:          sizeVC,
			allocator:       a,
			latencyVariable: sv,
		}
		vcs[i] = pc
	}

	listOfVC := VCList{
		VC:         vcs,
		localPort:  local,
		remotePort: remote,
	}

	return listOfVC
}

func (c *SwitchConnector) createOutComplex(
	local, remote sim.Port,
) outComplex {
	buf := buffering.NewBuffer(1)
	oc := outComplex{
		localPort:  local,
		remotePort: remote,
		outBuf:     buf,
	}
	return oc
}

func (c *SwitchConnector) createCreditComplex(
	local, remote sim.Port,
) creditComplex {
	cc := creditComplex{
		localPort:  local,
		remotePort: remote,
	}
	return cc
}

// ConnectEndPointToSwitch connects an EndPoint to a Switch.
func (c *SwitchConnector) ConnectEndPointToSwitch(
	ep *EndPoint,
	sw *Switch,
	freq sim.Freq,
	numVC int,
	sizeVC int,
) (switchPort sim.Port) {
	port := sim.NewLimitNumMsgPort(sw, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.Port%d", sw.Name(), len(sw.ports)))

	conn := sim.NewDirectConnection(
		fmt.Sprintf("%s-%s", ep.NetworkPort.Name(), port.Name()),
		c.engine, freq)
	conn.PlugIn(port, 1)
	conn.PlugIn(ep.NetworkPort, 1)

	sw.addPort(c.createVCList(port, ep.NetworkPort, numVC, sizeVC),
		c.createOutComplex(port, ep.NetworkPort), numVC, sizeVC)
	ep.DefaultSwitchDst = port
	ep.addPort(ep.NetworkPort, numVC, sizeVC)

	// Credit channel connection between an EndPoint and a RouteR
	creditPortA := sim.NewLimitNumMsgPort(sw, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.CreditPort%d", sw.Name(), len(sw.creditPorts)))
	creditPortB := sim.NewLimitNumMsgPort(ep, c.bufferSizeInNumFlit,
		fmt.Sprintf("%s.CreditPort%d", ep.Name(), len(ep.creditPorts)))
	creditConn := sim.NewDirectConnection(
		fmt.Sprintf("%s-%s", sw.Name(), ep.Name()),
		c.engine, freq)
	creditConn.PlugIn(creditPortA, 1)
	creditConn.PlugIn(creditPortB, 1)
	sw.addCreditPort(c.createCreditComplex(creditPortA, creditPortB),
		port)
	ep.addCreditPort(c.createCreditComplex(creditPortB, creditPortA),
		ep.NetworkPort)

	return port
}
