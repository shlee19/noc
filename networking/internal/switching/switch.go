package switching

import (
	"fmt"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/noc"

	"gitlab.com/akita/noc/v2/networking/internal/arbitration"
	"gitlab.com/akita/noc/v2/networking/internal/routing"

	"gitlab.com/akita/util/v2/buffering"
	"gitlab.com/akita/util/v2/pipelining"
)

type flitPipelineItem struct {
	taskID string
	flit   *noc.Flit
}

func (f flitPipelineItem) TaskID() string {
	return f.taskID
}

// A portComplex is the infrastructure related to a port.
type portComplex struct {
	// localPort is the port that is equipped on the switch.
	localPort sim.Port

	// remotePort is the port that is connected to the localPort.
	remotePort sim.Port

	// Data arrived at the local port needs to be processed in a pipeline. There
	// is a processing pipeline for each local port.
	pipeline pipelining.Pipeline

	// The flits here are buffered after the pipeline and are waiting to be
	// assigned with an output buffer.
	routeBuffer buffering.Buffer

	//The Head flits allocated the Virtual buffer number watching the credit of downstream switch
	//and allocate the virtual channel of downstream switch.
	virtualBuffer buffering.Buffer

	// The flits here are buffered to wait to be forwarded to the output buffer.
	forwardBuffer buffering.Buffer

	// The flits here are waiting to be sent to the next hop.
	sendOutBuffer buffering.Buffer

	sizeVC int

	allocator *vcAllocator

	latencyVariable *stageVariable
}

// VCList let you access to portComplex
// portComplex==VC  <-- correct concept, pls update later variable number
type VCList struct {
	VC           []portComplex
	localPort    sim.Port
	remotePort   sim.Port
	lastSelected int
}

// DownstreamVCInfo have information of credit and state.
type DownstreamVCInfo struct {
	credit []int
	state  []string
}

// creditComplex is the infrastructure related to a port for credit route.
type creditComplex struct {
	localPort  sim.Port
	remotePort sim.Port
}

// outComplex is the infarasturcture related to outBuf, outPort.
type outComplex struct {
	localPort  sim.Port
	remotePort sim.Port
	outBuf     buffering.Buffer
}

// Switch is an Akita component that can forward request to destination.
type Switch struct {
	*sim.TickingComponent

	ports                []sim.Port
	portToComplexMapping map[sim.Port]portComplex
	routingTable         routing.Table
	arbiter              arbitration.Arbiter

	degree               int
	sizeVC               int
	numVC                int
	portToVCList         map[sim.Port]VCList
	downstreamVCInfoList map[sim.Port]DownstreamVCInfo
	portToOutComplex     map[sim.Port]outComplex
	bufferToComplex      map[buffering.Buffer]portComplex

	// To make Route mapping
	forPortMapping int
	portMapping    map[int]sim.Port

	// credit channel connection.
	creditPorts                []sim.Port
	portToCreditComplexMapping map[sim.Port]creditComplex
}

// addPort create the port on switch.
func (s *Switch) addPort(list VCList, oc outComplex, numVC int, sizeVC int) {
	s.ports = append(s.ports, list.localPort)
	s.portToVCList[list.localPort] = list

	//To make creditTable
	credit := make([]int, numVC)
	state := make([]string, numVC)
	ct := DownstreamVCInfo{
		credit: credit,
		state:  state,
	}
	s.downstreamVCInfoList[list.localPort] = ct
	s.portToOutComplex[list.localPort] = oc
	for i := 0; i < numVC; i++ {
		s.arbiter.AddBuffer(list.VC[i].forwardBuffer)
		s.downstreamVCInfoList[list.localPort].credit[i] = sizeVC
		s.downstreamVCInfoList[list.localPort].state[i] = "IDLE"
		s.bufferToComplex[list.VC[i].forwardBuffer] = list.VC[i]
	}
}

//addCreditPort make the New route to credit or VCAllocate.
func (s *Switch) addCreditPort(complex creditComplex, port sim.Port) {
	s.creditPorts = append(s.creditPorts, complex.localPort)
	s.portToCreditComplexMapping[port] = complex
}

// GetRoutingTable returns the routine table used by the switch.
func (s *Switch) GetRoutingTable() routing.Table {
	return s.routingTable
}

// Tick update the Switch's state.
func (s *Switch) Tick(now sim.VTimeInSec) bool {
	madeProgress := false
	//For Flit Pipelined

	madeProgress = s.sendOut(now) || madeProgress
	madeProgress = s.forward(now) || madeProgress
	madeProgress = s.virtualAllocate(now) || madeProgress
	madeProgress = s.route(now) || madeProgress
	madeProgress = s.movePipeline(now) || madeProgress
	madeProgress = s.startProcessing(now) || madeProgress

	//For credit Check
	madeProgress = s.creditCheck(now) || madeProgress

	return madeProgress
}

// creditCheck is to check see if credit is available.
func (s *Switch) creditCheck(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.creditPorts {
		item := port.Peek()

		if item == nil {
			continue
		}

		credit := item.(*noc.Flit)
		vcNum := credit.UpstreamVCnum
		downstreamVCInfo := s.downstreamVCInfoList[credit.UpstreamPort]
		downstreamVCInfo.credit[vcNum] = downstreamVCInfo.credit[vcNum] + 1

		port.Retrieve(now)
		madeProgress = true
	}

	return madeProgress
}

func (s *Switch) startProcessing(now sim.VTimeInSec) (madeProgress bool) {
	//Find flit which go through switch from port.
	if s.forPortMapping == 0 {
		s.routeMapping()
		s.forPortMapping++
	}

	for _, port := range s.ports {
		item := port.Peek()
		if item == nil {
			continue
		}

		flit := item.(*noc.Flit)
		vcList := s.portToVCList[port]
		complex := vcList.VC[flit.VCnum]

		if !complex.pipeline.CanAccept() {
			continue
		}

		pipelineItem := flitPipelineItem{
			taskID: sim.GetIDGenerator().Generate(),
			flit:   item.(*noc.Flit),
		}

		complex.pipeline.Accept(now, pipelineItem)
		port.Retrieve(now)
		madeProgress = true
	}

	return madeProgress
}

func (s *Switch) movePipeline(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.ports {
		vcList := s.portToVCList[port]
		for i := 0; i < s.numVC; i++ {
			complex := vcList.VC[i]
			madeProgress = complex.pipeline.Tick(now) || madeProgress
		}
	}

	return madeProgress
}

func (s *Switch) route(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.ports {
		vcList := s.portToVCList[port]

		for i := 0; i < s.numVC; i++ {
			complex := vcList.VC[i]
			routeBuf := complex.routeBuffer
			virtualBuf := complex.virtualBuffer
			item := routeBuf.Peek()
			if item == nil {
				continue
			}
			if complex.latencyVariable.CheckRoute() == 0 {
				madeProgress = true
				continue
			}

			if !virtualBuf.CanPush() {
				continue
			}

			pipelineItem := item.(flitPipelineItem)
			flit := pipelineItem.flit

			s.assignFlitOutputBuf(flit)

			routeBuf.Pop()
			virtualBuf.Push(flit)
			madeProgress = true
		}
	}

	return madeProgress
}

func (s *Switch) virtualAllocate(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.ports {
		vcList := s.portToVCList[port]
		for i := 0; i < s.numVC; i++ {
			complex := vcList.VC[i]

			virtualBuf := complex.virtualBuffer
			forwardBuf := complex.forwardBuffer
			item := virtualBuf.Peek()
			if item == nil {
				continue
			}

			if complex.latencyVariable.CheckVirtualAllocate() == 0 {
				madeProgress = true
				continue
			}

			if !forwardBuf.CanPush() {
				continue
			}

			flit := item.(*noc.Flit)
			VC := s.assignFlitVC(flit, complex)
			if VC < 0 {
				continue
			}

			flit.UpstreamVCnum = flit.VCnum
			flit.VCnum = VC

			virtualBuf.Pop()
			forwardBuf.Push(flit)

			madeProgress = true
		}
	}
	return madeProgress
}

func (s *Switch) forward(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.ports {
		vcList := s.portToVCList[port]
		for i := 0; i < s.numVC; i++ {
			complex := vcList.VC[i]
			forwardBuf := complex.forwardBuffer
			item := forwardBuf.Peek()
			if item == nil {
				continue
			}
			flit := item.(*noc.Flit)
			downstreamVCInfo := s.downstreamVCInfoList[flit.OutputPort]
			downstreamCredit := downstreamVCInfo.credit[flit.VCnum]
			s.arbiter.UpdateDownStreamVCInfo(flit, downstreamCredit)
		}
	}

	inputBuffers := s.arbiter.Arbitrate(now)
	for _, buf := range inputBuffers {
		complex := s.bufferToComplex[buf]

		item := buf.Peek()
		if item == nil {
			continue
		}
		if complex.latencyVariable.CheckForward() == 0 {
			madeProgress = true
			continue
		}
		flit := item.(*noc.Flit)
		if !flit.OutputBuf.CanPush() {
			continue
		}
		downstreamVCInfo := s.downstreamVCInfoList[flit.OutputPort]
		if downstreamVCInfo.credit[flit.VCnum] <= 0 {
			continue
		}

		srcPortInRouter := flit.Dst
		departPortOfUpstreamRouter := flit.Src
		creditComplex := s.portToCreditComplexMapping[srcPortInRouter]
		src := creditComplex.localPort
		dst := creditComplex.remotePort
		credit := noc.FlitBuilder{}.
			WithSrc(src).
			WithDst(dst).
			WithSendTime(now).
			Build()
		credit.UpstreamPort = departPortOfUpstreamRouter
		credit.UpstreamVCnum = flit.UpstreamVCnum
		err := src.Send(credit)
		if err != nil {
			continue
		}

		downstreamVCInfo.credit[flit.VCnum] = downstreamVCInfo.credit[flit.VCnum] - 1
		if flit.FlitType == "TAIL" {
			downstreamVCInfo.state[flit.VCnum] = "IDLE"
		} else if flit.FlitType == "HAED" {
			downstreamVCInfo.state[flit.VCnum] = "ACTIVE"
		} else if flit.FlitType == "SIGNAL" {
			downstreamVCInfo.state[flit.VCnum] = "IDLE"
		}

		flit.OutputBuf.Push(flit)
		buf.Pop()
		madeProgress = true
	}

	return madeProgress
}

func (s *Switch) sendOut(now sim.VTimeInSec) (madeProgress bool) {
	for _, port := range s.ports {
		outComplex := s.portToOutComplex[port]
		sendOutBuf := outComplex.outBuf
		item := sendOutBuf.Peek()
		if item == nil {
			continue
		}
		flit := item.(*noc.Flit)

		flit.Meta().Src = outComplex.localPort
		flit.Meta().Dst = outComplex.remotePort
		flit.Meta().SendTime = now

		err := outComplex.localPort.Send(flit)
		if err == nil {
			sendOutBuf.Pop()
			madeProgress = true
		}
	}
	return madeProgress
}

func (s *Switch) assignFlitOutputBuf(f *noc.Flit) sim.Port {
	switchName := s.TickingComponent.Name()
	outPort := s.routingTable.FindPort(
		switchName, s.portMapping, f.Msg.Meta().Src, f.Msg.Meta().Dst, s.degree)

	outComplex := s.portToOutComplex[outPort]
	buffer := outComplex.outBuf
	f.OutputPort = outPort
	f.OutputBuf = buffer

	return outPort
}

// assignFlitVC, only HeadFlit pass through this stage.
func (s *Switch) assignFlitVC(f *noc.Flit, pc portComplex) int {
	VC := pc.allocator.allocate(f.OutputPort, f.FlitType, s.downstreamVCInfoList, s.numVC)
	return VC
}

func (s *Switch) setFlitNextHopDst(f *noc.Flit) {
	f.Src = f.Dst
	f.Dst = s.portToComplexMapping[f.Src].remotePort
}

func (s *Switch) routeMapping() {
	for _, port := range s.ports {
		vcList := s.portToVCList[port]
		portComplex := vcList.VC[0]
		s.portMapping[s.makeNumber(portComplex)] = portComplex.localPort
	}
}

// RouterBuilder can build switches
type SwitchBuilder struct {
	engine       sim.Engine
	freq         sim.Freq
	routingTable routing.Table
	arbiter      arbitration.Arbiter

	degree int
	numVC  int
	sizeVC int
}

// WithEngine sets the engine that the switch to build uses.
func (b SwitchBuilder) WithEngine(engine sim.Engine) SwitchBuilder {
	b.engine = engine
	return b
}

// WithFreq sets the frequency that the switch to build works at.
func (b SwitchBuilder) WithFreq(freq sim.Freq) SwitchBuilder {
	b.freq = freq
	return b
}

// WithRoutingTable sets the routing table to be used by the switch to build.
func (b SwitchBuilder) WithRoutingTable(rt routing.Table) SwitchBuilder {
	b.routingTable = rt
	return b
}

// WithArbiter sets the arbiter to be used by the switch to build.
func (b SwitchBuilder) WithArbiter(arbiter arbitration.Arbiter) SwitchBuilder {
	b.arbiter = arbiter
	return b
}

//WithDegree : number of switch per side. Degree*Degree topology
func (b SwitchBuilder) WithDegree(degree int) SwitchBuilder {
	b.degree = degree
	return b
}

//WithNumVC sets the Virtual channel num how much they allocated
func (b SwitchBuilder) WithNumVC(numVC int) SwitchBuilder {
	b.numVC = numVC
	return b
}

//WithSizeVC sets each size of virtual channel buffer size. later it can be
//set to credit number..
func (b SwitchBuilder) WithSizeVC(sizeVC int) SwitchBuilder {
	b.sizeVC = sizeVC
	return b
}

// Build creates a new switch
func (b SwitchBuilder) Build(name string) *Switch {
	b.engineMustBeGiven()
	b.freqMustNotBeZero()
	b.routingTableMustBeGiven()
	b.arbiterMustBeGiven()

	s := &Switch{}
	s.TickingComponent = sim.NewTickingComponent(name, b.engine, b.freq, s)
	s.routingTable = b.routingTable
	s.arbiter = b.arbiter
	s.portToVCList = make(map[sim.Port]VCList)
	s.portToComplexMapping = make(map[sim.Port]portComplex)

	s.degree = b.degree
	s.numVC = b.numVC
	s.sizeVC = b.sizeVC
	s.portMapping = make(map[int]sim.Port)
	s.portToOutComplex = make(map[sim.Port]outComplex)

	s.forPortMapping = 0
	s.downstreamVCInfoList = make(map[sim.Port]DownstreamVCInfo)
	s.portToCreditComplexMapping = make(map[sim.Port]creditComplex)

	s.bufferToComplex = make(map[buffering.Buffer]portComplex)
	return s
}

func (b SwitchBuilder) engineMustBeGiven() {
	if b.engine == nil {
		panic("engine of switch is not given")
	}
}

func (b SwitchBuilder) freqMustNotBeZero() {
	if b.freq == 0 {
		panic("switch frequency cannot be 0")
	}
}
func (b SwitchBuilder) routingTableMustBeGiven() {
	if b.routingTable == nil {
		panic("switch requires a routing table to operate")
	}
}

func (b SwitchBuilder) arbiterMustBeGiven() {
	if b.arbiter == nil {
		panic("switch requires an arbiter to operate")
	}
}

func (s *Switch) makeNumber(port portComplex) int {
	for i := 100; i >= 0; i-- {
		if strings.Contains(port.remotePort.Name(), fmt.Sprintf("Switch%d", i)) {
			return i
		}
	}
	return -1
}
