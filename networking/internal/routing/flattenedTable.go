package routing

import (
	"fmt"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
)

func NewFlattenedTable() Table {
	t := &flattenedTable{}

	t.t = make(map[sim.Port]sim.Port)
	t.numberToPort = make(map[int]sim.Port)
	t.GPU = false

	return t
}

type flattenedTable struct {
	t map[sim.Port]sim.Port

	numberToPort map[int]sim.Port
	defaultPort  sim.Port

	GPU bool

	numberRouters int
}

// FindPortFlattendGeneralMinimalHop minimal routing used inti Flattened
// Butterfly
func (t flattenedTable) FindPort(
	switchName string,
	portMapping map[int]sim.Port,
	src, dst sim.Port,
	n int,
) sim.Port {
	out, found := t.t[dst]
	if found {
		return out
	}

	id := t.TranslateNumber(switchName) - 1
	var destination int
	if t.GPU {
		destination = t.TranslateGPUPortNumber(dst) - 1
	} else {
		destination = t.TranslateAgentPortNumber(dst) - 1
	}

	if destination == -2 {
		return t.defaultPort
	}

	x1 := id / n
	y1 := id % n
	x2 := destination / n
	y2 := destination % n

	if x1 == x2 || y1 == y2 {
		return portMapping[destination+1]
	} else {
		return portMapping[n*x2+y1+1]
	}
}

func (t flattenedTable) TranslateNumber(name string) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(name, fmt.Sprintf("%d", i)) {
			return i
		}
	}

	return -1
}

func (t *flattenedTable) TranslateAgentPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("Agent%d", i)) {
			return i
		}
	}

	return -1
}

func (t *flattenedTable) TranslateGPUPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("GPU%d", i)) {
			return i
		}
	}

	return -1
}

func (t *flattenedTable) DefineRoute(finalDst, outputPort sim.Port) {
	t.t[finalDst] = outputPort
}

func (t *flattenedTable) DefineDefaultRoute(outputPort sim.Port) {
	t.defaultPort = outputPort
}

func (t *flattenedTable) DefineNumRouters(numRouters int) {
	t.numberRouters = numRouters
}

func (t *flattenedTable) DefineConnectGPU(GPU bool) {
	t.GPU = GPU
}
