package routing

import (
	"fmt"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
)

// NewTable creates a new Table.
func NewTable() Table {
	t := &table{}

	t.t = make(map[sim.Port]sim.Port)
	t.numberToPort = make(map[int]sim.Port)
	t.GPU = false

	return t
}

type table struct {
	t map[sim.Port]sim.Port

	numberToPort map[int]sim.Port
	defaultPort  sim.Port

	GPU bool

	numberRouters int
}

func (t table) FindPort(
	switchName string,
	portMapping map[int]sim.Port,
	src, dst sim.Port,
	n int,
) sim.Port {
	out, found := t.t[dst]
	if found {
		return out
	}
	return t.defaultPort
}

func (t table) TranslateNumber(name string) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(name, fmt.Sprintf("%d", i)) {
			return i
		}
	}
	return -1
}

func (t *table) TranslateAgentPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("Agent%d", i)) {
			return i
		}
	}
	return -1
}

func (t *table) TranslateGPUPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("GPU%d", i)) {
			return i
		}
	}
	return -1
}

func (t *table) DefineRoute(finalDst, outputPort sim.Port) {
	t.t[finalDst] = outputPort
}

func (t *table) DefineDefaultRoute(outputPort sim.Port) {
	t.defaultPort = outputPort
}

func (t *table) DefineNumRouters(numRouters int) {
	t.numberRouters = numRouters
}

func (t *table) DefineConnectGPU(GPU bool) {
	t.GPU = GPU
}
