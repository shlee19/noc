package routing

import (
	"fmt"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
)

func NewMeshTable() Table {
	t := &meshTable{}

	t.t = make(map[sim.Port]sim.Port)
	t.numberToPort = make(map[int]sim.Port)
	t.GPU = false

	return t
}

type meshTable struct {
	t map[sim.Port]sim.Port

	numberToPort map[int]sim.Port
	defaultPort  sim.Port

	GPU bool

	numberRouters int
}

// FindPortMeshGeneralMinimalHop finds the minimal routing used in mesh
// and called dor.
func (t meshTable) FindPort(
	switchName string,
	portMapping map[int]sim.Port,
	src, dst sim.Port,
	n int,
) sim.Port {
	out, found := t.t[dst]
	if found {
		return out
	}

	//t.GPU = true
	id := t.TranslateNumber(switchName) - 1
	var destination int
	if t.GPU {
		destination = t.TranslateGPUPortNumber(dst) - 1
	} else {
		destination = t.TranslateAgentPortNumber(dst) - 1
	}

	if destination == -2 {
		return t.defaultPort
	}

	x1 := id / n
	y1 := id % n
	x2 := destination / n
	y2 := destination % n

	if x2 > x1 {
		return portMapping[id+1+n]
	} else if x2 < x1 {
		return portMapping[id+1-n]
	} else if x2 == x1 {
		if y2 > y1 {
			return portMapping[id+1+1]
		} else if y2 < y1 {
			return portMapping[id+1-1]
		}
	}

	return t.defaultPort
}

func (t *meshTable) TranslateNumber(name string) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(name, fmt.Sprintf("%d", i)) {
			return i
		}
	}

	return -1
}

func (t *meshTable) TranslateAgentPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("Agent%d", i)) {
			return i
		}
	}

	return -1
}

func (t *meshTable) TranslateGPUPortNumber(dst sim.Port) int {
	for i := t.numberRouters; i >= 0; i-- {
		if strings.Contains(dst.Name(), fmt.Sprintf("GPU%d", i)) {
			return i
		}
	}

	return -1
}

func (t *meshTable) DefineRoute(finalDst, outputPort sim.Port) {
	t.t[finalDst] = outputPort
}

func (t *meshTable) DefineDefaultRoute(outputPort sim.Port) {
	t.defaultPort = outputPort
}

func (t *meshTable) DefineNumRouters(numRouters int) {
	t.numberRouters = numRouters
}

func (t *meshTable) DefineConnectGPU(GPU bool) {
	t.GPU = GPU
}
