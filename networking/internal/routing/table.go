package routing

import (
	"gitlab.com/akita/akita"
)

type Table interface {
	FindPort(switchName string,
		portMapping map[int]akita.Port,
		src, dst akita.Port,
		n int,
	) akita.Port

	DefineDefaultRoute(outputPort akita.Port)
	DefineNumRouters(numRouters int)
	DefineConnectGPU(GPU bool)

	TranslateNumber(name string) int
	TranslateAgentPortNumber(dst akita.Port) int
	DefineRoute(finalDst, outputPort akita.Port)

	/*
		FindPortMeshGeneralMinimalHop(
			switchName string,
			portMapping map[int]akita.Port,
			src, dst akita.Port,
			n int,
		) akita.Port
		FindPortFlattendGeneralMinimalHop(
			switchName string,
			portMapping map[int]akita.Port,
			src, dst akita.Port,
			n int,
		) akita.Port
	*/
}
