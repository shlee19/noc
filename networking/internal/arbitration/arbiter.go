package arbitration

import (
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/noc"
	"gitlab.com/akita/util/v2/buffering"
)

// Arbiter can determine which buffer can send a message out
type Arbiter interface {
	// Add a buffer for arbitration
	AddBuffer(buf buffering.Buffer)

	// Arbitrate returns a set of ports that can send request in the next cycle.
	Arbitrate(now sim.VTimeInSec) []buffering.Buffer

	//UpdateDownstreamVCInfo update the downstreamVC information.
	UpdateDownStreamVCInfo(flit *noc.Flit, downstreamCredit int)
}
