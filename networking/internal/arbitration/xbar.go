package arbitration

import (
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/noc"
	"gitlab.com/akita/util/v2/buffering"
)

// NewXBarArbiter creates a new XBar arbiter.
func NewXBarArbiter() Arbiter {
	xbar := &xbarArbiter{}
	xbar.creditTable = make(map[*noc.Flit]int)

	return xbar
}

type xbarArbiter struct {
	buffers    []buffering.Buffer
	nextPortID int

	creditTable map[*noc.Flit]int
}

func (a *xbarArbiter) AddBuffer(buf buffering.Buffer) {
	a.buffers = append(a.buffers, buf)
}

func (a *xbarArbiter) UpdateDownStreamVCInfo(flit *noc.Flit, downstreamCredit int) {
	a.creditTable[flit] = downstreamCredit
}

func (a *xbarArbiter) Arbitrate(now sim.VTimeInSec) []buffering.Buffer {
	startingPortID := a.nextPortID
	selectedPort := make([]buffering.Buffer, 0)
	occupiedOutputPort := make(map[buffering.Buffer]bool)

	for i := 0; i < len(a.buffers); i++ {
		currPortID := (startingPortID + i) % len(a.buffers)
		buf := a.buffers[currPortID]
		item := buf.Peek()
		if item == nil {
			continue
		}
		flit := item.(*noc.Flit)

		if a.creditTable[flit] <= 0 {
			continue
		}

		if _, ok := occupiedOutputPort[flit.OutputBuf]; ok {
			continue
		}

		selectedPort = append(selectedPort, buf)
		occupiedOutputPort[flit.OutputBuf] = true
	}

	a.nextPortID = (a.nextPortID + 1) % len(a.buffers)

	return selectedPort
}
