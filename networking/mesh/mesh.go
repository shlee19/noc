// Package mesh provides a Connector and establishes a mesh connection.
package mesh

import (
	"fmt"
	"math"
	"strings"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/networking/internal/arbitration"
	"gitlab.com/akita/noc/v2/networking/internal/networking"
	"gitlab.com/akita/noc/v2/networking/internal/routing"
	"gitlab.com/akita/noc/v2/networking/internal/switching"
)

type switchNode struct {
	parentSwitchNode   *switchNode
	currSwitch         *switching.Switch
	localPortToParent  sim.Port
	remotePortToParent sim.Port
}

// Connector can connect devices into a Mesh network.
type MeshConnector struct {
	networkName      string
	engine           sim.Engine
	freq             sim.Freq
	encodingOverhead float64
	flitByteSize     int
	switchLatency    int
	switchConnector  *switching.SwitchConnector
	network          *networking.NetworkedConnection
	switchNodes      []*switchNode

	meshSwitches []*switching.Switch
	endPoints    []*switching.EndPoint

	config Config

	creditTable *int
	degree      int
	numVC       int
	sizeVC      int
	GPU         bool
}

// NewConnector creates a new connector that can help configure Mesh networks.
func NewConnector() *MeshConnector {
	c := &MeshConnector{}
	c = c.WithVersion3().
		WithX16().
		WithSwitchLatency(1)
	return c
}

// WithEngine sets the event-driven simulation engine that the Mesh connection
// uses.
func (c *MeshConnector) WithEngine(engine sim.Engine) *MeshConnector {
	c.engine = engine
	return c
}

// WithVersion3 defines the Mesh connection to use the Mesh-v3 standard.
func (c *MeshConnector) WithVersion3() *MeshConnector {
	c.freq = 1 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

// WithVersion4 defines the Mesh connection to use the Mesh-v4 standard.
func (c *MeshConnector) WithVersion4() *MeshConnector {
	c.freq = 2 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

// WithSwitchLatency (shlee)this latency related with each stage in switch
func (c *MeshConnector) WithSwitchLatency(numCycles int) *MeshConnector {
	c.switchLatency = numCycles
	return c
}

// WithX1 sets Mesh connection to a X1 Mesh connection.
func (c *MeshConnector) WithX1() *MeshConnector {
	c.flitByteSize = 1
	return c
}

// WithX2 sets Mesh connection to a X1 Mesh connection.
func (c *MeshConnector) WithX2() *MeshConnector {
	c.flitByteSize = 2
	return c
}

// WithX4 sets Mesh connection to a X1 Mesh connection.
func (c *MeshConnector) WithX4() *MeshConnector {
	c.flitByteSize = 4
	return c
}

// WithX8 sets Mesh connection to a X1 Mesh connection.
func (c *MeshConnector) WithX8() *MeshConnector {
	c.flitByteSize = 8
	return c
}

// WithX16 sets Mesh connection to a X1 Mesh connection.
func (c *MeshConnector) WithX16() *MeshConnector {
	c.flitByteSize = 16
	return c
}

// WithNetworkName sets the name of the network and the prefix of all the
// component in the network.
func (c *MeshConnector) WithNetworkName(name string) *MeshConnector {
	c.networkName = name
	return c
}

// WithDegree : Width of topology, number of switch per side.
func (c *MeshConnector) WithDegree(degree int) *MeshConnector {
	c.degree = degree
	return c
}

// WithNumVC : number of VC
func (c *MeshConnector) WithNumVC(numVC int) *MeshConnector {
	c.numVC = numVC
	return c
}

// WithSizeVC : Legnth of VC buffer
func (c *MeshConnector) WithSizeVC(sizeVC int) *MeshConnector {
	c.sizeVC = sizeVC
	return c
}

// WithConfig gives the config of router
func (c *MeshConnector) WithConfig(config Config) *MeshConnector {
	c.config = config
	return c
}

func (c *MeshConnector) WithGPU(GPU bool) *MeshConnector {
	c.GPU = GPU
	return c
}

func (c *MeshConnector) RouterAddress(routerNum int) *switching.Switch {
	return c.meshSwitches[routerNum]
}

// CreateNetwork creates a network. This function should be called before
// creating root complexes.
func (c *MeshConnector) CreateNetwork() {
	c.network = networking.NewNetworkedConnection()
	c.switchConnector = switching.NewSwitchConnector(c.engine)
	c.switchConnector.SetSwitchLatency(c.switchLatency)
	//Set the latency of stage in router stage.
	//c.switchConnector.SetRouteLatency(c.config.RouteLatency)
	//c.switchConnector.SetvaLatency(c.config.VirtualAllocateLatency)
	//c.switchConnector.SetForwardLatency(c.config.ForwardLatency)
	c.switchConnector.SetRouteLatency(1)
	c.switchConnector.SetvaLatency(1)
	c.switchConnector.SetForwardLatency(1)
}

func (c *MeshConnector) routeRootComplexToCPU(
	rc *switching.Switch,
	rcPort sim.Port,
	cpuPorts []sim.Port,
) {
	rt := rc.GetRoutingTable()

	for _, p := range cpuPorts {
		rt.DefineRoute(p, rcPort)
	}

	rt.DefineNumRouters(c.degree * c.degree)
}

// AddSwitch adds a new switch connecting from an existing switch.
func (c *MeshConnector) AddMeshSwitch() (switchID int) {
	switchID = len(c.meshSwitches) + 1
	sw := switching.SwitchBuilder{}.
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithRoutingTable(routing.NewMeshTable()).
		WithArbiter(arbitration.NewXBarArbiter()).
		WithDegree(c.degree).
		WithNumVC(c.numVC).
		Build(fmt.Sprintf("%s.Switch%d", c.networkName, switchID))
	c.network.AddSwitch(sw)

	c.meshSwitches = append(c.meshSwitches, sw)
	c.switchNodes = append(c.switchNodes, &switchNode{
		currSwitch: sw,
	})

	return switchID
}

// ConnectBetweenSwitches is to connect the switch between normal switch
func (c *MeshConnector) ConnectBetweenSwitches(Switch1 int, Switch2 int) {
	baseSwitch := c.meshSwitches[Switch1-1]
	connectSwitch := c.meshSwitches[Switch2-1]

	basePort, newPort := c.switchConnector.
		ConnectSwitches(baseSwitch, connectSwitch, c.freq, c.numVC, c.sizeVC)

	connectSwitch.GetRoutingTable().DefineDefaultRoute(newPort)
	c.switchNodes = append(c.switchNodes, &switchNode{
		parentSwitchNode:   c.switchNodes[Switch1-1],
		currSwitch:         connectSwitch,
		localPortToParent:  newPort,
		remotePortToParent: basePort,
	})
}

// PlugInDevice connects a series of ports to a switch.
func (c *MeshConnector) PlugInMesh(meshSwitchID int, devicePorts []sim.Port) {
	endPoint := switching.MakeEndPointBuilder().
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithFlitByteSize(c.flitByteSize).
		WithEncodingOverhead(c.encodingOverhead).
		WithDevicePorts(devicePorts).
		WithNumVC(c.numVC).
		WithSizeVC(c.sizeVC).
		Build(fmt.Sprintf("%s.EndPoint%d", c.networkName, len(c.endPoints)))
	c.endPoints = append(c.endPoints, endPoint)
	c.network.AddEndPoint(endPoint)

	baseSwitch := c.meshSwitches[meshSwitchID]
	port := c.switchConnector.
		ConnectEndPointToSwitch(endPoint, baseSwitch, c.freq, c.numVC, c.sizeVC)

	sn := c.switchNodes[meshSwitchID]
	c.establishRouteToDevice(sn, port, devicePorts)
}

func (c *MeshConnector) establishRouteToDevice(
	sn *switchNode,
	port sim.Port,
	devicePorts []sim.Port,
) {
	if sn == nil {
		return
	}

	rt := sn.currSwitch.GetRoutingTable()
	for _, dst := range devicePorts {
		rt.DefineRoute(dst, port)

		if strings.Contains(dst.Name(), "GPU") {
			sn.currSwitch.GetRoutingTable().DefineConnectGPU(true)
		}
	}

	rt.DefineNumRouters(c.degree * c.degree)
	c.establishRouteToDevice(sn.parentSwitchNode,
		sn.remotePortToParent, devicePorts)
}

func (c *MeshConnector) ConnectMesh(meshConnector *MeshConnector, n int) {
	if n < 2 {
		return
	}
	for i := 0; i < n; i++ {
		for j := 1; j < n; j++ {
			meshConnector.ConnectBetweenSwitches((n)*i+j, (n)*i+j+1)
		}
	}
	for i := 0; i < n; i++ {
		for j := 1; j < n; j++ {
			meshConnector.ConnectBetweenSwitches(i+(n*(j-1))+1, i+(n*j)+1)
		}
	}
}

func (c *MeshConnector) ConnectMesh_kn(meshConnector *MeshConnector, k int, n int) {
	//nodes = pow(k, n)
	if k < 2 {
		return
	}
	network_size := math.Pow(k, n) //network_size : number of switches
	for i := 0; i < network_size; i++ {
		if i%k != k-1 {
			for j := 0; j < 0; j++ {
				meshConnector.ConnectBetweenSwitches(i, i+math.Pow(k, j))
			}
		}
	}

}
