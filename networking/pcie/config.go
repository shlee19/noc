package pcie

import (
	"flag"
	"fmt"
)

type Config struct {
	Ratio    int
	Topology int
	Routing  int
	Degree   int
	NumVC    int
	SizeVC   int

	RouteLatency           int
	VirtualAllocateLatency int
	ForwardLatency         int

	Interval int
}

func (c *Config) Setting() {
	var ratio = flag.Int("ratio", 100, "inject ratio(0~100)")
	var topology = flag.Int("topology", 2, "topology")
	var routing = flag.Int("routing", 1, "routing algorithm")
	var degree = flag.Int("degree", 8, "degree")
	var numVC = flag.Int("numVC", 4, "number of Virtual Channel")
	var sizeVC = flag.Int("sizeVC", 16, "buffer size on Virtual Channel")

	var routeLatency = flag.Int("routeLatency", 1, "route stage latency")
	var virtualAllocateLatency = flag.Int("virtualAllocateLatency", 1,
		"virtual Allocate stage latency")
	var forwardLatency = flag.Int("forwardLatency", 1, "forward stage latency")

	var interval = flag.Int("interval", 3000, "interval check")

	flag.Parse()

	c.setRatio(*ratio)
	c.setTopology(*topology)
	c.setRouting(*routing)
	c.setDegree(*degree)
	c.setNumVC(*numVC)
	c.setSizeVC(*sizeVC)
	c.setRouteLatency(*routeLatency)
	c.setVirtualAllocateLatency(*virtualAllocateLatency)
	c.setForwardLatency(*forwardLatency)
	c.setInterval(*interval)

	fmt.Printf("Ratio : %0.2f     ", float64(*ratio)/float64(100))
	//fmt.Printf("interval : %0.2f ns \n", float64(*interval))
}

func (c *Config) setRatio(ratio int) {
	c.Ratio = ratio
}

func (c *Config) setTopology(topology int) {
	c.Topology = topology
}

func (c *Config) setRouting(routing int) {
	c.Routing = routing
}

func (c *Config) setDegree(degree int) {
	c.Degree = degree
}

func (c *Config) setNumVC(numVC int) {
	c.NumVC = numVC
}

func (c *Config) setSizeVC(sizeVC int) {
	c.SizeVC = sizeVC
}

func (c *Config) setRouteLatency(routeLatency int) {
	c.RouteLatency = routeLatency
}

func (c *Config) setVirtualAllocateLatency(virtualAllocateLatency int) {
	c.VirtualAllocateLatency = virtualAllocateLatency
}

func (c *Config) setForwardLatency(forwardLatency int) {
	c.ForwardLatency = forwardLatency
}

func (c *Config) setInterval(interval int) {
	c.Interval = interval
}
