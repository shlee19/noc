// Package pcie provides a Connector and establishes a PCIe connection.
package pcie

import (
	"fmt"
	//	"strings"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/networking/internal/arbitration"
	"gitlab.com/akita/noc/v2/networking/internal/networking"
	"gitlab.com/akita/noc/v2/networking/internal/routing"
	"gitlab.com/akita/noc/v2/networking/internal/switching"
)

type switchNode struct {
	parentSwitchNode   *switchNode
	currSwitch         *switching.Switch
	localPortToParent  sim.Port
	remotePortToParent sim.Port
}

// Connector can connect devices into a PCIe network.
type Connector struct {
	networkName      string
	engine           sim.Engine
	freq             sim.Freq
	encodingOverhead float64
	flitByteSize     int
	switchLatency    int
	switchConnector  *switching.SwitchConnector
	network          *networking.NetworkedConnection
	switches         []*switching.Switch
	rootComplexes    []*switching.Switch
	switchNodes      []*switchNode // The same switches, but as trees.
	endPoints        []*switching.EndPoint

	config Config

	creditTable *int
	degree      int
	numVC       int
	sizeVC      int
	GPU         bool
}

// NewConnector creates a new connector that can help configure PCIe networks.
func NewConnector() *Connector {
	c := &Connector{}
	c = c.WithVersion3().
		WithX16().
		WithSwitchLatency(1)
	return c
}

// WithEngine sets the event-driven simulation engine that the PCIe connection
// uses.
func (c *Connector) WithEngine(engine sim.Engine) *Connector {
	c.engine = engine
	return c
}

// WithVersion3 defines the PCIe connection to use the PCIe-v3 standard.
func (c *Connector) WithVersion3() *Connector {
	c.freq = 1 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

// WithVersion4 defines the PCIe connection to use the PCIe-v4 standard.
func (c *Connector) WithVersion4() *Connector {
	c.freq = 2 * sim.GHz
	c.encodingOverhead = (130 - 128) / 130
	return c
}

//WithSwitchLatency (shlee)this latency related with each stage in switch
func (c *Connector) WithSwitchLatency(numCycles int) *Connector {
	c.switchLatency = numCycles
	return c
}

// WithX1 sets PCIe connection to a X1 PCIe connection.
func (c *Connector) WithX1() *Connector {
	c.flitByteSize = 1
	return c
}

// WithX2 sets PCIe connection to a X1 PCIe connection.
func (c *Connector) WithX2() *Connector {
	c.flitByteSize = 2
	return c
}

// WithX4 sets PCIe connection to a X1 PCIe connection.
func (c *Connector) WithX4() *Connector {
	c.flitByteSize = 4
	return c
}

// WithX8 sets PCIe connection to a X1 PCIe connection.
func (c *Connector) WithX8() *Connector {
	c.flitByteSize = 8
	return c
}

// WithX16 sets PCIe connection to a X1 PCIe connection.
func (c *Connector) WithX16() *Connector {
	c.flitByteSize = 16
	return c
}

// WithNetworkName sets the name of the network and the prefix of all the
// component in the network.
func (c *Connector) WithNetworkName(name string) *Connector {
	c.networkName = name
	return c
}

//WithDegree : Width of topology, number of switch per side.
func (c *Connector) WithDegree(degree int) *Connector {
	c.degree = degree
	return c
}

//WithNumVC : number of VC
func (c *Connector) WithNumVC(numVC int) *Connector {
	c.numVC = numVC
	return c
}

//WithSizeVC : Legnth of VC buffer
func (c *Connector) WithSizeVC(sizeVC int) *Connector {
	c.sizeVC = sizeVC
	return c
}

//WithConfig gives the config of router
func (c *Connector) WithConfig(config Config) *Connector {
	c.config = config
	return c
}

func (c *Connector) WithGPU(GPU bool) *Connector {
	c.GPU = GPU
	return c
}

func (c *Connector) RouterAddress(routerNum int) *switching.Switch {
	return c.switches[routerNum]
}

// CreateNetwork creates a network. This function should be called before
// creating root complexes.
func (c *Connector) CreateNetwork() {
	c.network = networking.NewNetworkedConnection()
	c.switchConnector = switching.NewSwitchConnector(c.engine)
	c.switchConnector.SetSwitchLatency(c.switchLatency)
	//Set the latency of stage in router stage.
	//c.switchConnector.SetRouteLatency(c.config.RouteLatency)
	//c.switchConnector.SetvaLatency(c.config.VirtualAllocateLatency)
	//c.switchConnector.SetForwardLatency(c.config.ForwardLatency)
	c.switchConnector.SetRouteLatency(1)
	c.switchConnector.SetvaLatency(1)
	c.switchConnector.SetForwardLatency(1)
}

// CreateRootComplex creates a root complex of the PCIe connection. It requires
// a set of port that connects to the CPU in the system.
func (c *Connector) CreateRootComplex(cpuPorts []sim.Port) (switchID int) {
	cpuEndPoint := switching.MakeEndPointBuilder().
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithDevicePorts(cpuPorts).
		WithFlitByteSize(c.flitByteSize).
		WithEncodingOverhead(c.encodingOverhead).
		WithNumVC(c.numVC).
		WithSizeVC(c.sizeVC).
		Build(c.networkName + ".CPUEndPoint")
	c.network.AddEndPoint(cpuEndPoint)
	c.endPoints = append(c.endPoints, cpuEndPoint)

	rootComplexSwitch := switching.SwitchBuilder{}.
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithArbiter(arbitration.NewXBarArbiter()).
		WithRoutingTable(routing.NewTable()).
		WithNumVC(c.numVC).
		Build(c.networkName + ".RootComplex")
	c.network.AddSwitch(rootComplexSwitch)
	c.switches = append(c.switches, rootComplexSwitch)
	c.rootComplexes = append(c.switches, rootComplexSwitch)
	c.switchNodes = append(c.switchNodes, &switchNode{
		parentSwitchNode:   nil,
		currSwitch:         rootComplexSwitch,
		localPortToParent:  nil,
		remotePortToParent: nil,
	})
	port := c.switchConnector.
		ConnectEndPointToSwitch(cpuEndPoint, rootComplexSwitch, c.freq, c.numVC, c.sizeVC)

	c.routeRootComplexToCPU(rootComplexSwitch, port, cpuPorts)

	return 0
}

func (c *Connector) routeRootComplexToCPU(
	rc *switching.Switch,
	rcPort sim.Port,
	cpuPorts []sim.Port,
) {
	rt := rc.GetRoutingTable()

	for _, p := range cpuPorts {
		rt.DefineRoute(p, rcPort)
	}

	rt.DefineNumRouters(c.degree * c.degree)
}

// AddSwitch adds a new switch connecting from an existing switch.
func (c *Connector) AddSwitch(baseSwitchID int) (switchID int) {
	switchID = len(c.switches)
	sw := switching.SwitchBuilder{}.
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithRoutingTable(routing.NewTable()).
		WithArbiter(arbitration.NewXBarArbiter()).
		WithDegree(c.degree).
		WithNumVC(c.numVC).
		Build(fmt.Sprintf("%s.Switch%d", c.networkName, switchID))
	c.network.AddSwitch(sw)

	c.switches = append(c.switches, sw)

	baseSwitch := c.switches[baseSwitchID]

	basePort, newPort := c.switchConnector.
		ConnectSwitches(baseSwitch, sw, c.freq, c.numVC, c.sizeVC)

	sw.GetRoutingTable().DefineDefaultRoute(newPort)
	c.switchNodes = append(c.switchNodes, &switchNode{
		parentSwitchNode:   c.switchNodes[baseSwitchID],
		currSwitch:         sw,
		localPortToParent:  newPort,
		remotePortToParent: basePort,
	})

	return switchID
}

// ConnectBetweenSwitches is to connect the switch between normal switch
func (c *Connector) ConnectBetweenSwitches(Switch1 int, Switch2 int) {
	baseSwitch := c.switches[Switch1]
	connectSwitch := c.switches[Switch2]

	basePort, newPort := c.switchConnector.
		ConnectSwitches(baseSwitch, connectSwitch, c.freq, c.numVC, c.sizeVC)

	connectSwitch.GetRoutingTable().DefineDefaultRoute(newPort)
	c.switchNodes = append(c.switchNodes, &switchNode{
		parentSwitchNode:   c.switchNodes[Switch1],
		currSwitch:         connectSwitch,
		localPortToParent:  newPort,
		remotePortToParent: basePort,
	})
}

// PlugInDevice connects a series of ports to a switch.
func (c *Connector) PlugInDevice(baseSwitchID int, devicePorts []sim.Port) {
	endPoint := switching.MakeEndPointBuilder().
		WithEngine(c.engine).
		WithFreq(c.freq).
		WithFlitByteSize(c.flitByteSize).
		WithEncodingOverhead(c.encodingOverhead).
		WithDevicePorts(devicePorts).
		WithNumVC(c.numVC).
		WithSizeVC(c.sizeVC).
		Build(fmt.Sprintf("%s.EndPoint%d", c.networkName, len(c.endPoints)))
	c.endPoints = append(c.endPoints, endPoint)
	c.network.AddEndPoint(endPoint)

	baseSwitch := c.switches[baseSwitchID]
	port := c.switchConnector.
		ConnectEndPointToSwitch(endPoint, baseSwitch, c.freq, c.numVC, c.sizeVC)

	sn := c.switchNodes[baseSwitchID]
	c.establishRouteToDevice(sn, port, devicePorts)
}

func (c *Connector) establishRouteToDevice(
	sn *switchNode,
	port sim.Port,
	devicePorts []sim.Port,
) {
	if sn == nil {
		return
	}

	rt := sn.currSwitch.GetRoutingTable()
	for _, dst := range devicePorts {
		rt.DefineRoute(dst, port)
	}

	rt.DefineNumRouters(c.degree * c.degree)
	c.establishRouteToDevice(sn.parentSwitchNode,
		sn.remotePortToParent, devicePorts)
}
