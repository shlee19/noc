package noc

import (
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	akita "gitlab.com/akita/akita/v2"
)

//go:generate mockgen -package=$GOPACKAGE -destination mock_akita_test.go -write_package_comment=false gitlab.com/akita/akita/v2 Engine,Port

func TestNOC(t *testing.T) {
	log.SetOutput(GinkgoWriter)
	RegisterFailHandler(Fail)
	RunSpecs(t, "GCN3")
}

type sendRecvAgent struct {
	*akita.TickingComponent

	out akita.Port

	sendLeft             int
	sendCount, recvCount int
}

func (a *sendRecvAgent) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	if a.sendLeft > 0 {
		msg := akita.Msg{
			SendTime:         now,
			Src:              a.out,
			Dst:              a.out,
			TransferByteSize: 64,
		}

		ok := a.out.Send(msg)
		if ok {
			a.sendLeft--
			a.sendCount++
			madeProgress = true
		}
	}

	_, received := a.out.Retrieve(now)
	if received {
		a.recvCount++
		madeProgress = true
	}

	return madeProgress
}

func newSendRecvAgent(
	name string,
	engine akita.EventSchedulingEngine,
) *sendRecvAgent {
	agent := new(sendRecvAgent)
	agent.TickingComponent = akita.NewTickingComponent(
		name, engine, 1*akita.GHz, agent)
	agent.out = akita.NewLimitNumMsgPort(name+".port", agent, 4096)
	return agent
}
