package noc

import akita "gitlab.com/akita/akita/v2"


// MsgBuffer is a buffer for messages
type MsgBuffer struct {
	Capacity int
	Buf      []akita.Msg
	vc       int
}

func (b *MsgBuffer) enqueue(req akita.Msg) {
	if len(b.Buf) > b.Capacity {
		panic("buffer overflow")
	}

	b.Buf = append(b.Buf, req)
}
