package noc

import (
	"log"
	"sync"

	akita "gitlab.com/akita/akita/v2"
)

// FixedBandwidthConnection models a connection that transfers messages at a
// speed of a certain number of bytes per cycle
type FixedBandwidthConnection struct {
	sync.Mutex
	akita.HookableBase

	name           string
	eventScheduler akita.EventScheduler
	tickScheduler  *akita.TickScheduler

	BytesPerCycle int
	NumLanes      int
	busy          int

	Freq              akita.Freq
	SrcBufferCapacity int
	DstBufferCapacity int

	srcBuffers         []*MsgBuffer
	srcPortToBufferMap map[akita.Port]*MsgBuffer
	srcBufferBusy      map[*MsgBuffer]bool
	lastSelectedSrcBuf int

	dstBuffers         []*MsgBuffer
	dstBufferToPortMap map[*MsgBuffer]akita.Port
	dstPortToBufferMap map[akita.Port]*MsgBuffer
	dstBusy            map[akita.Port]bool
	dstNumArrivingMsgs map[akita.Port]int

	needTick bool
}

// Name returns the name of the connection
func (c *FixedBandwidthConnection) Name() string {
	return c.name
}

// Handle handles events scheduled on the FixedBandwidthConnection
func (c *FixedBandwidthConnection) Handle(e akita.Event) {
	now := e.Time
	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosBeforeEvent,
		Item:   e,
	}
	c.InvokeHook(ctx)

	c.Lock()

	switch e.EventType {
	case akita.EventTypeTick:
		c.tick(now)
	case EventTypeTransfer:
		c.doTransfer(e)
	default:
		log.Panicf("cannot handle event of type %s", e.EventType.Name)
	}

	c.Unlock()

	ctx.Pos = akita.HookPosAfterEvent
	c.InvokeHook(ctx)
}

func (c *FixedBandwidthConnection) tick(now akita.VTimeInSec) {
	c.needTick = false

	c.doDeliver(now)
	c.scheduleTransfer(now)

	if c.needTick {
		c.tickScheduler.TickLater(now)
	}
}

func (c *FixedBandwidthConnection) doDeliver(now akita.VTimeInSec) {
	for _, buf := range c.dstBuffers {
		dst := c.dstBufferToPortMap[buf]

		if c.dstBusy[dst] {
			continue
		}

		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		msg.RecvTime = now
		ok := dst.Recv(msg)
		if ok {
			buf.Buf = buf.Buf[1:]
			c.needTick = true

			ctx := akita.HookCtx{
				Domain: c,
				Now:    now,
				Pos:    akita.HookPosConnDeliver,
				Item:   msg,
			}
			c.InvokeHook(ctx)

			// log.Printf("%.15f: Msg %s delivered", now, msg.GetID())
		} else {
			c.dstBusy[dst] = true
		}
	}
}

func (c *FixedBandwidthConnection) scheduleTransfer(now akita.VTimeInSec) {
	if c.busy >= c.NumLanes {
		return
	}

	for i := 0; i < len(c.srcBuffers); i++ {

		bufIndex := (i + c.lastSelectedSrcBuf + 1) % len(c.srcBuffers)

		buf := c.srcBuffers[bufIndex]
		if c.srcBufferBusy[buf] {
			continue
		}
		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		dst := msg.Dst
		dstBuf := c.dstPortToBufferMap[dst]
		if len(dstBuf.Buf)+c.dstNumArrivingMsgs[dst] >= c.DstBufferCapacity {
			continue
		}

		c.needTick = true
		cycles := ((msg.TransferByteSize - 1) / c.BytesPerCycle) + 1
		transferTime := c.Freq.NCyclesLater(cycles, now)
		transferEvent := TransferEventBuilder{}.
			WithMsg(msg).
			Build()
		transferEvent.Time = transferTime
		transferEvent.Handler = c
		c.dstNumArrivingMsgs[dst]++
		c.eventScheduler.Schedule(transferEvent)

		ctx := akita.HookCtx{
			Domain: c,
			Now:    now,
			Pos:    akita.HookPosConnStartTrans,
			Item:   msg,
		}
		c.InvokeHook(ctx)

		c.busy++

		c.srcBufferBusy[buf] = true
		c.lastSelectedSrcBuf = bufIndex
	}
}

func (c *FixedBandwidthConnection) doTransfer(evt akita.Event) {
	now := evt.Time
	info := evt.Info.(TransferEventInfo)
	msg := info.msg
	src := msg.Src
	dst := msg.Dst

	srcBuf := c.srcPortToBufferMap[src]
	dstBuf := c.dstPortToBufferMap[dst]

	srcBuf.Buf = srcBuf.Buf[1:]
	dstBuf.Buf = append(dstBuf.Buf, msg)

	c.busy--
	c.srcBufferBusy[srcBuf] = false
	c.dstNumArrivingMsgs[dst]--
	c.tickScheduler.TickLater(now)

	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosConnDoneTrans,
		Item:   msg,
	}
	c.InvokeHook(ctx)

	src.NotifyAvailable(evt.Time)
}

// Send starts message transfer. It guarantees that the message get delievered
// sometime later. It may also return an error indicating that this message
// cannot be sent at the moment.
func (c *FixedBandwidthConnection) Send(msg akita.Msg) bool {
	c.Lock()
	defer c.Unlock()

	buf := c.srcPortToBufferMap[msg.Src]

	if buf == nil {
		log.Panic("not connected")
	}

	if len(buf.Buf) >= c.SrcBufferCapacity {
		return false
	}

	buf.Buf = append(buf.Buf, msg)
	c.tickScheduler.TickLater(msg.SendTime)

	ctx := akita.HookCtx{
		Domain: c,
		Now:    msg.SendTime,
		Pos:    akita.HookPosConnStartSend,
		Item:   msg,
	}
	c.InvokeHook(ctx)

	return true
}

// PlugIn connects a port to the FixedBandwidthConnection
func (c *FixedBandwidthConnection) PlugIn(port akita.Port) {
	_, connected := c.srcPortToBufferMap[port]
	if connected {
		log.Panic("port already connected")
	}

	srcBuf := &MsgBuffer{
		Capacity: c.SrcBufferCapacity,
	}
	c.srcBuffers = append(c.srcBuffers, srcBuf)
	c.srcPortToBufferMap[port] = srcBuf
	c.srcBufferBusy[srcBuf] = false

	dstBuf := &MsgBuffer{
		Capacity: c.DstBufferCapacity,
	}
	c.dstBuffers = append(c.dstBuffers, dstBuf)
	c.dstBufferToPortMap[dstBuf] = port
	c.dstPortToBufferMap[port] = dstBuf
	c.dstNumArrivingMsgs[port] = 0
	c.dstBusy[port] = false

	port.SetConnection(c)
}

// Unplug disconnect the connection with a port
func (c *FixedBandwidthConnection) Unplug(port akita.Port) {
	panic("implement me")
}

// NotifyAvailable enables the FixedBandwidthConnection to continue try to
// deliver to a desination
func (c *FixedBandwidthConnection) NotifyAvailable(
	now akita.VTimeInSec,
	port akita.Port,
) {
	c.Lock()
	c.dstBusy[port] = false
	c.tickScheduler.TickLater(now)
	c.Unlock()
}

// NewFixedBandwidthConnection creates a new FixedBandwidthConnection
func NewFixedBandwidthConnection(
	bytesPerCycle int,
	eventScheduler akita.EventScheduler,
	freq akita.Freq,
) *FixedBandwidthConnection {
	conn := new(FixedBandwidthConnection)

	conn.BytesPerCycle = bytesPerCycle
	conn.SrcBufferCapacity = 1
	conn.DstBufferCapacity = 1
	conn.srcPortToBufferMap = make(map[akita.Port]*MsgBuffer)
	conn.srcBufferBusy = make(map[*MsgBuffer]bool)
	conn.dstBufferToPortMap = make(map[*MsgBuffer]akita.Port)
	conn.dstPortToBufferMap = make(map[akita.Port]*MsgBuffer)
	conn.dstNumArrivingMsgs = make(map[akita.Port]int)
	conn.dstBusy = make(map[akita.Port]bool)

	conn.Freq = freq
	conn.eventScheduler = eventScheduler
	conn.tickScheduler = akita.NewTickScheduler(conn,
		eventScheduler, conn.Freq)

	conn.NumLanes = 1

	return conn
}
