package noc

import (
	"gitlab.com/akita/akita"
)

type directChannelEnd struct {
	port    akita.Port
	buf     []akita.Msg
	bufSize int
	busy    bool
	tempBuf []akita.Msg
}

// Channel connects two components without latency
type Channel struct {
	*akita.TickingComponent

	engine         akita.Engine
	nextPortID     int
	ports          []akita.Port
	ends           map[akita.Port]*directChannelEnd
	channelLatency int
	msgLatencyInfo map[akita.Msg]int
}

// PlugIn marks the port connects to this Channel.
func (c *Channel) PlugIn(port akita.Port, sourceSideBufSize int) {
	c.Lock()
	defer c.Unlock()

	c.ports = append(c.ports, port)
	end := &directChannelEnd{}
	end.port = port
	end.bufSize = c.channelLatency
	c.ends[port] = end

	port.SetConnection(c)
}

// Unplug marks the port no longer connects to this Channel.
func (c *Channel) Unplug(port akita.Port) {
	panic("not implemented")
}

// NotifyAvailable is called by a port to notify that the connection can
// deliver to the port again.
func (c *Channel) NotifyAvailable(now akita.VTimeInSec, port akita.Port) {
	c.TickNow(now)
}

// Send of a Channel schedules a DeliveryEvent immediately
func (c *Channel) Send(msg akita.Msg) *akita.SendError {
	c.Lock()
	defer c.Unlock()

	c.msgMustBeValid(msg)

	srcEnd := c.ends[msg.Meta().Src]

	if len(srcEnd.tempBuf) >= srcEnd.bufSize {
		srcEnd.busy = true
		return akita.NewSendError()
	}

	srcEnd.tempBuf = append(srcEnd.tempBuf, msg)
	c.msgLatencyInfo[msg] = 0

	c.TickNow(msg.Meta().SendTime)

	return nil
}

func (c *Channel) msgMustBeValid(msg akita.Msg) {
	c.portMustNotBeNil(msg.Meta().Src)
	c.portMustNotBeNil(msg.Meta().Dst)
	c.portMustBeConnected(msg.Meta().Src)
	c.portMustBeConnected(msg.Meta().Dst)
	c.srcDstMustNotBeTheSame(msg)
}

func (c *Channel) portMustNotBeNil(port akita.Port) {
	if port == nil {
		panic("src or dst is not given")
	}
}

func (c *Channel) portMustBeConnected(port akita.Port) {
	if _, connected := c.ends[port]; !connected {
		panic("src or dst is not connected")
	}
}

func (c *Channel) srcDstMustNotBeTheSame(msg akita.Msg) {
	if msg.Meta().Src == msg.Meta().Dst {
		panic("sending back to src")
	}
}

func (c *Channel) Tick(now akita.VTimeInSec) bool {
	madeProgress := false
	for i := 0; i < len(c.ports); i++ {
		portID := (i + c.nextPortID) % len(c.ports)
		port := c.ports[portID]
		end := c.ends[port]

		for _, msg := range end.tempBuf {
			c.msgLatencyInfo[msg] = c.msgLatencyInfo[msg] + 1

			if c.msgLatencyInfo[msg] == c.channelLatency {
				end.buf = append(end.buf, msg)
				end.tempBuf = end.tempBuf[1:]
				delete(c.msgLatencyInfo, msg)
				madeProgress = c.forwardMany(end, now) || madeProgress

			} else if len(end.tempBuf) == 0 {
				madeProgress = false

			} else {
				madeProgress = true

			}
		}
	}
	c.nextPortID = (c.nextPortID + 1) % len(c.ports)
	return madeProgress
}

func (c *Channel) forwardMany(
	end *directChannelEnd,
	now akita.VTimeInSec,
) bool {
	madeProgress := false
	for {
		if len(end.buf) == 0 {
			break
		}

		head := end.buf[0]
		head.Meta().RecvTime = now

		err := head.Meta().Dst.Recv(head)
		if err != nil {
			break
		}

		madeProgress = true
		end.buf = end.buf[1:]

		if end.busy {
			end.port.NotifyAvailable(now)
			end.busy = false
		}
	}

	return madeProgress
}

// NewChannel creates a new Channel object
func NewChannel(
	name string,
	engine akita.Engine,
	freq akita.Freq,
	channelLatency int,
) *Channel {
	c := new(Channel)
	c.TickingComponent = akita.NewSecondaryTickingComponent(name, engine, freq, c)
	c.ends = make(map[akita.Port]*directChannelEnd)
	c.channelLatency = channelLatency
	c.msgLatencyInfo = make(map[akita.Msg]int)

	return c
}
