package main

import (
	"fmt"

	"gitlab.com/akita/akita/v2"
	"gitlab.com/akita/noc/v2"
	"gitlab.com/akita/noc/v2/standalone"
)

func main() {
	engine := akita.EngineBuilder{}.Build()
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	trafficInjector := standalone.NewGreedyTrafficInjector(engine)
	trafficInjector.PacketSize = 4096

	trafficCounter := new(noc.TrafficCounter)
	conn := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)
	conn.AcceptHook(trafficCounter)

	var agents []*standalone.Agent
	for i := 0; i < 2; i++ {
		name := fmt.Sprintf("agent_%d", i)
		agent := standalone.NewAgent(name, engine)
		trafficInjector.RegisterAgent(agent)
		conn.PlugIn(agent.ToOut)

		agents = append(agents, agent)
	}

	trafficInjector.InjectTraffic()

	engine.Run()

	fmt.Printf("Total data sent over interconnect %d\n",
		trafficCounter.TotalData)
	fmt.Printf("Bandwidth achieved %f GB/s\n",
		float64(trafficCounter.TotalData)/float64(engine.CurrentTime())/1e9)

}
