package noc

import akita "gitlab.com/akita/akita/v2"


// EventTypeTransfer is the type for event that transfers a message
var EventTypeTransfer = &akita.EventType{
	Name: "gitlab.com/akita/noc.EventTypeTransfer",
}

// TransferEventInfo carries the information for the transfer event
type TransferEventInfo struct {
	msg akita.Msg
	vc  int
}

// TransferEventBuilder can build transfer event
type TransferEventBuilder struct {
	msg akita.Msg
	vc  int
}


// WithMsg sets the message to be transferred on the event
func (b TransferEventBuilder) WithMsg(msg akita.Msg) TransferEventBuilder {
	b.msg = msg
	return b
}

// WithVC sets the transfer event will transfer on which virtual channel
func (b TransferEventBuilder) WithVC(vc int) TransferEventBuilder {
	b.vc = vc
	return b
}

// Build build the transfer event
func (b TransferEventBuilder) Build() akita.Event {
	event := akita.EventBuilder{}.
		WithEventType(EventTypeTransfer).
		WithInfo(TransferEventInfo{
			msg: b.msg,
		}).
		Build()
	return event
}
