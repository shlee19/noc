package noc

import (
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/util/v2/buffering"
)

// Flit is the smallest trasferring unit on a network.
type Flit struct {
	sim.MsgMeta
	SeqID        int
	NumFlitInMsg int
	Msg          sim.Msg
	OutputBuf    buffering.Buffer

	sendFlitOut float64
	recvFlitIn  float64

	Signal       int
	Intermediate int

	FlitType      string
	VCnum         int
	InputPort     sim.Port
	inputVC       int
	OutputPort    sim.Port
	UpstreamPort  sim.Port
	UpstreamVCnum int
}

// Meta returns the meta data assocated with the Flit.
func (f *Flit) Meta() *sim.MsgMeta {
	return &f.MsgMeta
}

// FlitBuilder can build flits
type FlitBuilder struct {
	sendTime            sim.VTimeInSec
	src, dst            sim.Port
	msg                 sim.Msg
	seqID, numFlitInMsg int
	sendFlitOut         float64
}

// WithSendTime sets the send time of the request to build
func (b FlitBuilder) WithSendTime(t sim.VTimeInSec) FlitBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the src of the request to send
func (b FlitBuilder) WithSrc(src sim.Port) FlitBuilder {
	b.src = src
	return b
}

// WithDst sets the dst of the request to send
func (b FlitBuilder) WithDst(dst sim.Port) FlitBuilder {
	b.dst = dst
	return b
}

// WithSeqID sets the SeqID of the Flit.
func (b FlitBuilder) WithSeqID(i int) FlitBuilder {
	b.seqID = i
	return b
}

// WithNumFlitInMsg sets the NumFlitInMsg for of flit to build.
func (b FlitBuilder) WithNumFlitInMsg(n int) FlitBuilder {
	b.numFlitInMsg = n
	return b
}

// WithMsg sets the msg of the flit to build.
func (b FlitBuilder) WithMsg(msg sim.Msg) FlitBuilder {
	b.msg = msg
	return b
}
func (b FlitBuilder) WithsendFlitOut(time float64) FlitBuilder {
	b.sendFlitOut = time
	return b
}

// Build creates a new flit.
func (b FlitBuilder) Build() *Flit {
	f := &Flit{}
	f.SendTime = b.sendTime
	f.Src = b.src
	f.Dst = b.dst
	f.Msg = b.msg
	f.SeqID = b.seqID
	f.NumFlitInMsg = b.numFlitInMsg
	f.Signal = 0
	f.Intermediate = 0
	f.sendFlitOut = b.sendFlitOut

	return f
}
