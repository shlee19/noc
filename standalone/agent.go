package standalone

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita/v2"
)


// MsgTypeTraffic is the type of messages that are only used in standalone
// network test.
var MsgTypeTraffic = &akita.MsgType{Name: "Traffic"}

// MakeTrafficMsg creates a new traffic message
func MakeTrafficMsg(
	src, dst akita.Port,
	byteSize int,
	trafficClass int,
) akita.Msg {
	msg := akita.MsgBuilder{}.
		WithMsgType(MsgTypeTraffic).
		WithSrc(src).
		WithDst(dst).
		WithTransferByteSize(byteSize).
		WithTrafficClass(trafficClass).
		Build()
	return msg
}

// EventTypeStartSending is the event type that tells the agent to send a message to the destination.
var EventTypeStartSending = &akita.EventType{Name: "StartSending"}

// StartSendingEventInfo is the information that attach to start sending event.
type StartSendingEventInfo = struct {
	msg akita.Msg
}

// MakeStartSendingEvent creates a new start sending event
func MakeStartSendingEvent(
	time akita.VTimeInSec,
	src, dst *Agent,
	byteSize int,
	trafficClass int,

) akita.Event {
	msg := MakeTrafficMsg(src.ToOut, dst.ToOut, byteSize, trafficClass)
	evt := akita.EventBuilder{}.
		WithTime(time).
		WithEventType(EventTypeStartSending).
		WithInfo(StartSendingEventInfo{msg: msg}).
		Build()
	return evt
}

// Agent is a component that connects the network. It can send and receive
// msguests to/ from the network.
type Agent struct {
	*akita.ComponentBase
	*akita.TickScheduler

	ToOut akita.Port


	Buffer   []akita.Msg
	needTick bool
}

// NotifyRecv is used by the port to notify the agent that a message is arrived.
func (a *Agent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	a.ToOut.Retrieve(now)
	a.TickLater(now)
}


// NotifyPortFree notifies that a port is free and can accept new out-going
// messages
func (a *Agent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	a.TickLater(now)
}

// Handle defines how the agent handles events
func (a *Agent) Handle(e akita.Event) {
	switch e.EventType {
	case EventTypeStartSending:
		a.handleStartSendEvent(e)
	case akita.EventTypeTick:
		a.tick(e)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(e))
	}
}


func (a *Agent) handleStartSendEvent(e akita.Event) {
	info := e.Info.(StartSendingEventInfo)
	a.Buffer = append(a.Buffer, info.msg)
	a.TickLater(e.Time)
}

func (a *Agent) tick(e akita.Event) {
	now := e.Time
	a.needTick = false

	a.sendDataOut(now)

	if a.needTick {
		a.TickLater(now)
	}
}

func (a *Agent) sendDataOut(now akita.VTimeInSec) bool {
	if len(a.Buffer) == 0 {
		return false
	}

	msg := a.Buffer[0]

	msg.SendTime = now
	ok := a.ToOut.Send(msg)
	if ok {
		a.Buffer = a.Buffer[1:]
		a.needTick = true
	}
	return false
}


// NewAgent creates a new Agent
func NewAgent(name string, eventScheduler akita.EventScheduler) *Agent {
	a := new(Agent)
	a.ComponentBase = akita.NewComponentBase(name)
	a.TickScheduler = akita.NewTickScheduler(a, eventScheduler, 1*akita.GHz)


	a.ToOut = akita.NewLimitNumMsgPort(name+".ToOut", a, 4)

	return a
}
