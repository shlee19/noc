package standalone

import (
	"math/rand"

	"gitlab.com/akita/akita/v2"
)


// A TrafficInjector can create random traffic with predefined traffic patterns.
type TrafficInjector interface {
	RegisterAgent(a *Agent)
	InjectTraffic()
}

// GreedyTrafficInjector generate a large number of traffic at the beginning of
// the simulation.
type GreedyTrafficInjector struct {
	agents []*Agent

	scheduler akita.EventScheduler

	PacketSize int
	NumPackets int
}


// NewGreedyTrafficInjector creates a new GreedyTrafficInjector
func NewGreedyTrafficInjector(
	scheduler akita.EventScheduler,
) *GreedyTrafficInjector {
	ti := new(GreedyTrafficInjector)
	ti.PacketSize = 1024
	ti.NumPackets = 1024
	ti.scheduler = scheduler
	return ti
}


// RegisterAgent registers an agent to the traffic generator so that the traffic
// generator can generate traffic from/ to the agent.
func (ti *GreedyTrafficInjector) RegisterAgent(a *Agent) {
	ti.agents = append(ti.agents, a)
}


// InjectTraffic creates randomized traffic in the network.
func (ti *GreedyTrafficInjector) InjectTraffic() {
	for i, a := range ti.agents {
		for j := 0; j < ti.NumPackets; j++ {
			dstID := rand.Int() % (len(ti.agents) - 1)
			if dstID >= i {
				dstID++
			}
			dst := ti.agents[dstID]

			pkt := MakeStartSendingEvent(0, a, dst, ti.PacketSize, j)
			ti.scheduler.Schedule(pkt)
		}
	}
}
