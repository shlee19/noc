module gitlab.com/akita/noc/v2

require (
	// replace gitlab.com/akita/akita => ../akita

	// replace gitlab.com/akita/util => ../util

	github.com/aws/aws-sdk-go v1.37.7 // indirect
	github.com/golang/mock v1.4.4
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/onsi/ginkgo v1.15.0
	github.com/onsi/gomega v1.10.5
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tebeka/atexit v0.3.0
	gitlab.com/akita/akita/v2 v2.0.0-alpha.2
	gitlab.com/akita/util/v2 v2.0.0-alpha.3
	go.mongodb.org/mongo-driver v1.4.6 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect

)

go 1.13
