package main

import (
	"fmt"
	"math/rand"

	"github.com/tebeka/atexit"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/acceptance"
	flattened "gitlab.com/akita/noc/v2/networking/flattenedButterfly"
)

func main() {
	config := &flattened.Config{}
	config.Setting()

	rand.Seed(1)

	engine := sim.NewSerialEngine()
	t := acceptance.NewTest(config.Interval)

	createFlattenedNetwork(engine, t, *config)

	engine.Run()
	atexit.Exit(0)
}

func createFlattenedNetwork(
	engine sim.Engine,
	test *acceptance.Test,
	config flattened.Config,
) {
	freq := 1.0 * sim.GHz
	var agents []*acceptance.Agent
	var nodes int = config.Degree * config.Degree
	trafficManager := acceptance.NewTrafficManager()
	trafficManager.Degree = config.Degree
	trafficManager.Flattened = true

	for i := 1; i < nodes+1; i++ {
		agent := acceptance.NewAgent(
			engine, freq, fmt.Sprintf("Agent%d", i),
			5, test, config.Ratio, &trafficManager.StopSignal)
		agent.TickLater(0)
		agents = append(agents, agent)

		agent.TrafficManager = trafficManager
		trafficManager.AddAgent(agent)
	}

	FlattenedConnector := flattened.NewConnector()
	FlattenedConnector = FlattenedConnector.
		WithEngine(engine).
		WithNetworkName("Flattened").
		WithSwitchLatency(1).
		WithVersion3().
		WithX16().
		WithDegree(config.Degree).
		WithNumVC(config.NumVC).
		WithSizeVC(config.SizeVC).
		WithConfig(config)
	FlattenedConnector.CreateNetwork()

	for i := 0; i < nodes; i++ {
		FlattenedConnector.AddFlattenedSwitch()
	}

	FlattenedConnector.ConnectFlattendButterfly(FlattenedConnector, config.Degree)

	for i := 0; i < nodes; i++ {
		FlattenedConnector.PlugInFlattened(i, agents[i].Ports)
	}

	for i := 0; i < nodes; i++ {
		test.RegisterAgent(agents[i])
	}
}
