package acceptance

import (
	"fmt"
	"log"
	"math/rand"

	"gitlab.com/akita/akita/v2/sim"
)

type trafficMsg struct {
	sim.MsgMeta
}

func (m *trafficMsg) Meta() *sim.MsgMeta {
	return &m.MsgMeta
}

// Test is a test case.
type Test struct {
	agents            []*Agent
	msgs              []sim.Msg
	receivedMsgs      []sim.Msg
	receivedMsgsTable map[sim.Msg]bool

	specimenMsgs []sim.Msg
	interval     float64
}

// NewTest creates a new test.
func NewTest(interval int) *Test {
	t := &Test{}
	t.receivedMsgsTable = make(map[sim.Msg]bool)

	t.interval = float64(interval) / 1e9
	return t
}

// RegisterAgent adds an agent to the Test
func (t *Test) RegisterAgent(agent *Agent) {
	t.agents = append(t.agents, agent)
}

// GenerateMsgs generates n message from a random source port to a random
// destination port.
func (t *Test) GenerateMsgs(n uint64) {
	for i := uint64(0); i < n; i++ {
		srcAgentID := rand.Intn(len(t.agents))
		//srcAgentID := 0
		srcAgent := t.agents[srcAgentID]
		srcPortID := rand.Intn(len(srcAgent.Ports))
		srcPort := srcAgent.Ports[srcPortID]

		dstAgentID := rand.Intn(len(t.agents))
		//dstAgentID := 3
		for dstAgentID == srcAgentID {
			dstAgentID = rand.Intn(len(t.agents))
		}
		//fmt.Printf("srcAgentID  : %d, DstAgentID : %d \n", srcAgentID, dstAgentID)

		dstAgent := t.agents[dstAgentID]
		dstPortID := rand.Intn(len(dstAgent.Ports))
		dstPort := dstAgent.Ports[dstPortID]

		msg := &trafficMsg{}
		msg.Meta().ID = sim.GetIDGenerator().Generate()
		msg.Src = srcPort
		msg.Dst = dstPort
		//fmt.Printf("%s -> %s \n", msg.Src.Name(), msg.Dst.Name())
		msg.TrafficBytes = 4096 //4KB
		srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
		t.registerMsg(msg)
	}
}

// UniformRandomTraffic is randomly choose src and dst agents.
func (t *Test) UniformRandomTraffic() {
	for i := 1; i < 100000; i++ {
		//Src Agent
		srcAgentID := rand.Intn(len(t.agents)-1) + 1
		srcAgent := t.agents[srcAgentID]
		srcPortID := rand.Intn(len(srcAgent.Ports))
		srcPort := srcAgent.Ports[srcPortID]
		//Dst Agent
		dstAgentID := rand.Intn(len(t.agents)-1) + 1
		for dstAgentID == srcAgentID {
			dstAgentID = rand.Intn(len(t.agents)-1) + 1
		}
		dstAgent := t.agents[dstAgentID]
		dstPortID := rand.Intn(len(dstAgent.Ports))
		dstPort := dstAgent.Ports[dstPortID]
		//Msg Contents
		msg := &trafficMsg{}
		msg.Meta().ID = sim.GetIDGenerator().Generate()
		msg.Src = srcPort
		msg.Dst = dstPort
		msg.TrafficBytes = 1
		srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
		t.registerMsg(msg)
	}
}

// WorstTraffic concentrate to specific destination agent.
func (t *Test) WorstTraffic() {
	for i := 1; i < 1000; i++ {
		// Src Agent
		srcAgentID := rand.Intn(len(t.agents)-1) + 1
		srcAgent := t.agents[srcAgentID]
		srcPortID := rand.Intn(len(srcAgent.Ports))
		srcPort := srcAgent.Ports[srcPortID]

		dstAgentID := len(t.agents) - srcAgentID

		dstAgent := t.agents[dstAgentID]
		dstPortID := rand.Intn(len(dstAgent.Ports))
		dstPort := dstAgent.Ports[dstPortID]

		msg := &trafficMsg{}
		msg.Meta().ID = sim.GetIDGenerator().Generate()
		msg.Src = srcPort
		msg.Dst = dstPort
		msg.TrafficBytes = 4
		srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
		t.registerMsg(msg)
	}
}
func (t *Test) HotSpot() {
	for i := 1; i < 1000; i++ {

		srcAgentID := rand.Intn(len(t.agents)-1) + 1
		srcAgent := t.agents[srcAgentID]
		srcPortID := rand.Intn(len(srcAgent.Ports))
		srcPort := srcAgent.Ports[srcPortID]

		dstAgentID := 1
		dstAgent := t.agents[dstAgentID]
		dstPortID := rand.Intn(len(dstAgent.Ports))
		dstPort := dstAgent.Ports[dstPortID]

		msg := &trafficMsg{}
		msg.Meta().ID = sim.GetIDGenerator().Generate()
		msg.Src = srcPort
		msg.Dst = dstPort
		msg.TrafficBytes = rand.Intn(128)
		srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
		t.registerMsg(msg)
	}
}

// GenerateSpecificMsg sent the msg from src i to dst j.
func (t *Test) GenerateSpecificMsg(i int, j int, count int) {
	for k := 0; k < count; k++ {
		srcAgent := t.agents[i]
		srcPortID := rand.Intn(len(srcAgent.Ports))
		srcPort := srcAgent.Ports[srcPortID]

		dstAgent := t.agents[j]
		dstPortID := rand.Intn(len(dstAgent.Ports))
		dstPort := dstAgent.Ports[dstPortID]

		msg := &trafficMsg{}
		msg.Meta().ID = sim.GetIDGenerator().Generate()
		msg.Src = srcPort
		msg.Dst = dstPort
		msg.TrafficBytes = rand.Intn(4096)
		srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
		t.registerMsg(msg)
	}
}

// GenerateAllCaseTraffic to check see if all msg can arrive to properly on destination.
func (t *Test) GenerateAllCaseTraffic() {
	for i := 1; i < len(t.agents); i++ {
		//Src Agent
		for j := 1; j < len(t.agents); j++ {
			srcAgent := t.agents[i]
			srcPortID := rand.Intn(len(srcAgent.Ports))
			srcPort := srcAgent.Ports[srcPortID]

			if i == j {
				continue
			}
			dstAgent := t.agents[j]
			dstPortID := rand.Intn(len(dstAgent.Ports))
			dstPort := dstAgent.Ports[dstPortID]

			msg := &trafficMsg{}
			msg.Meta().ID = sim.GetIDGenerator().Generate()
			msg.Src = srcPort
			msg.Dst = dstPort
			msg.TrafficBytes = rand.Intn(8)
			srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)
			t.registerMsg(msg)
		}
	}
}

func (t *Test) registerMsg(msg sim.Msg) {
	t.msgs = append(t.msgs, msg)
}

// receiveMsg marks that a message is received.
func (t *Test) receiveMsg(msg sim.Msg, recvPort sim.Port) {
	t.msgMustBeReceivedAtItsDestination(msg, recvPort)
	t.msgMustNotBeReceivedBefore(msg)
	t.receivedMsgs = append(t.receivedMsgs, msg)

	t.specimenMsgs = append(t.specimenMsgs, msg)
	// log.Printf("Msg %s: sent at %.10f, recved at %.10f",
	// 	msg.Meta().ID, msg.Meta().SendTime, msg.Meta().RecvTime)
}

func (t *Test) msgMustBeReceivedAtItsDestination(
	msg sim.Msg,
	recvPort sim.Port,
) {
	if msg.Meta().Dst != recvPort {
		panic("msg delivered to a wrong destination")
	}
}

func (t *Test) msgMustNotBeReceivedBefore(msg sim.Msg) {
	if _, found := t.receivedMsgsTable[msg]; found {
		panic("msg is double delivered")
	}
	t.receivedMsgsTable[msg] = true
}

// MustHaveReceivedAllMsgs asserts that all the messages sent are received.
func (t *Test) MustHaveReceivedAllMsgs() {
	if len(t.msgs) == len(t.receivedMsgs) {
		return
	}

	for _, sentMsg := range t.msgs {
		if _, found := t.receivedMsgsTable[sentMsg]; !found {
			log.Printf("msg %s expected, but not received\n", sentMsg.Meta().ID)
			src := sentMsg.Meta().Src.Name()
			dst := sentMsg.Meta().Dst.Name()
			fmt.Printf("msg INFO : Src: %s Dst: %s \n\n", src, dst)
		}
	}
	panic("some messages are dropped")
}

// ReportBandwidthAchieved dumps the bandwidth observed by each agents.
func (t *Test) ReportBandwidthAchieved(now sim.VTimeInSec) {
	for _, a := range t.agents {
		log.Printf("agent %s, send bandwidth %.2f GB/s, recv bandwidth %.2f GB/s",
			a.Name(),
			float64(a.sendBytes)/float64(now)/1e9,
			float64(a.recvBytes)/float64(now)/1e9)
	}
}

// LatencyComputation is to check the msg specification.
func (t *Test) LatencyComputation() {
	latency := float64(0)
	var lastMsg sim.Msg
	for _, receiveMsg := range t.receivedMsgs {
		tempLatency := float64(receiveMsg.Meta().RecvTime) - float64(receiveMsg.Meta().SendTime)
		latency = latency + tempLatency
		lastMsg = receiveMsg

		fmt.Printf("%0.2f ns \n", receiveMsg.Meta().SendTime*1e9)
		fmt.Printf("%0.2f ns \n", receiveMsg.Meta().RecvTime*1e9)
	}
	lastTime := lastMsg.Meta().RecvTime
	iter := float64(lastTime) / t.interval
	var tmpMsgs []sim.Msg
	var convergeLatency float64
	for i := 0; i < int(iter)+1; i++ {
		for _, receiveMsg := range t.receivedMsgs {
			recvTime := float64(receiveMsg.Meta().RecvTime)
			if recvTime > t.interval*float64(i) && recvTime < t.interval*float64(i+1) {
				tmpMsgs = append(tmpMsgs, receiveMsg)
			}
		}
		for _, Msg := range tmpMsgs {
			tempLatency := float64(Msg.Meta().RecvTime) - float64(Msg.Meta().SendTime)
			convergeLatency = convergeLatency + tempLatency
		}
		time := convergeLatency / float64(len(tmpMsgs))
		fmt.Printf("%f \n", time)
		tmpMsgs = nil
		convergeLatency = 0
	}
}
