package acceptance

import (
	"fmt"
	"math/rand"
	"strings"

	//"gitlab.com/sim"
	"gitlab.com/akita/akita/v2/sim"
)

// Agent can send and receive request.
type Agent struct {
	*sim.TickingComponent
	test       *Test
	Ports      []sim.Port
	MsgsToSend []sim.Msg
	sendBytes  uint64
	recvBytes  uint64

	ratio                  int
	saveID                 string
	InjectionQueue         []sim.Msg
	interval               float64
	tempTime               float64
	previousAverageLatency float64
	Stop                   *bool
	TrafficManager         *TrafficManager
}

// NewAgent creates a new agent.
func NewAgent(
	engine sim.Engine,
	freq sim.Freq,
	name string,
	numPorts int,
	test *Test,
	ratio int,
	stopSignal *bool,
) *Agent {
	a := &Agent{}
	a.test = test
	a.TickingComponent = sim.NewTickingComponent(name, engine, freq, a)

	a.ratio = ratio
	a.Stop = stopSignal
	for i := 0; i < numPorts; i++ {
		p := sim.NewLimitNumMsgPort(a, 1, fmt.Sprintf("%s.Port%d", name, i))
		a.Ports = append(a.Ports, p)
	}
	return a
}

// Tick tries to receive requests and send requests out.
func (a *Agent) Tick(now sim.VTimeInSec) bool {
	madeProgress := false
	if *a.Stop == true {
		return false
	}
	madeProgress = a.traffic(now) || madeProgress
	madeProgress = a.send(now) || madeProgress
	madeProgress = a.sendToNetwork(now) || madeProgress
	madeProgress = a.recv(now) || madeProgress
	return madeProgress
}

func (a *Agent) traffic(now sim.VTimeInSec) bool {
	if strings.Contains(a.TickingComponent.Name(), "Agent0") {
		return false
	}
	if *a.Stop == true {
		return false
	}
	a.TrafficManager.UniformRandom(a)
	return true
}

func (a *Agent) send(now sim.VTimeInSec) bool {
	if len(a.MsgsToSend) == 0 {
		return false
	}
	randomNumber := rand.Intn(100)
	if len(a.MsgsToSend) != 0 {
		if randomNumber < a.ratio {
			msg := a.MsgsToSend[0]
			msg.Meta().SendTime = now
			a.InjectionQueue = append(a.InjectionQueue, msg)
			a.MsgsToSend = a.MsgsToSend[1:]
			return true
		}
	}
	return true
}

func (a *Agent) sendToNetwork(now sim.VTimeInSec) bool {
	if len(a.InjectionQueue) == 0 {
		return false
	}
	msg := a.InjectionQueue[0]
	err := msg.Meta().Src.Send(msg)
	if err == nil {
		a.InjectionQueue = a.InjectionQueue[1:]
		a.sendBytes += uint64(msg.Meta().TrafficBytes)
		return true
	}
	return false
}

func (a *Agent) recv(now sim.VTimeInSec) bool {
	madeProgress := false

	for _, port := range a.Ports {
		msg := port.Retrieve(now)
		if msg != nil {
			a.test.receiveMsg(msg, port)
			a.recvBytes += uint64(msg.Meta().TrafficBytes)
			madeProgress = true
			a.TrafficManager.StopCheck(a, msg)
		}
	}

	return madeProgress
}
