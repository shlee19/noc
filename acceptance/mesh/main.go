package main

import (
	"fmt"
	"math/rand"

	"github.com/tebeka/atexit"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc/acceptance"
	"gitlab.com/akita/noc/networking/mesh"
)

func main() {
	config := mesh.Config{}
	config.Setting()

	rand.Seed(1)

	engine := akita.NewSerialEngine()
	t := acceptance.NewTest(config.Interval)

	createMeshNetwork(engine, t, config)

	engine.Run()
	atexit.Exit(0)
}
func createMeshNetwork(
	engine akita.Engine,
	test *acceptance.Test,
	config mesh.Config,
) {
	freq := 1.0 * akita.GHz
	var agents []*acceptance.Agent
	var nodes int = config.Degree * config.Degree
	trafficManager := acceptance.NewTrafficManager()
	trafficManager.Degree = config.Degree
	trafficManager.Mesh = true

	for i := 1; i < nodes+1; i++ {
		agent := acceptance.NewAgent(
			engine, freq, fmt.Sprintf("Agent%d", i),
			5, test, config.Ratio, &trafficManager.StopSignal)
		agent.TickLater(0)
		agents = append(agents, agent)

		agent.TrafficManager = trafficManager
		trafficManager.AddAgent(agent)
	}

	meshConnector := mesh.NewConnector()
	meshConnector = meshConnector.
		WithEngine(engine).
		WithNetworkName("Mesh").
		WithSwitchLatency(1).
		WithVersion3().
		WithX16().
		WithDegree(config.Degree).
		WithNumVC(config.NumVC).
		WithSizeVC(config.SizeVC).
		WithConfig(config)
	meshConnector.CreateNetwork()

	for i := 0; i < nodes; i++ {
		meshConnector.AddMeshSwitch()
	}

	meshConnector.ConnectMesh(meshConnector, config.Degree)

	for i := 0; i < nodes; i++ {
		meshConnector.PlugInMesh(i, agents[i].Ports)
	}

	for i := 0; i < nodes; i++ {
		test.RegisterAgent(agents[i])
	}
}
