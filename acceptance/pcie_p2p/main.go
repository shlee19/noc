package main

import (
	"fmt"
	"math/rand"

	"github.com/tebeka/atexit"
	"gitlab.com/akita/akita/v2/sim"
	"gitlab.com/akita/noc/v2/acceptance"
	"gitlab.com/akita/noc/v2/networking/pcie"
)

func main() {
	config := &pcie.Config{}
	config.Setting()
	rand.Seed(1)
	engine := sim.NewSerialEngine()
	t := acceptance.NewTest(config.Interval)

	createNetwork(engine, t, *config)
	engine.Run()

	//t.MustHaveReceivedAllMsgs()
	//t.ReportBandwidthAchieved(engine.CurrentTime())
	atexit.Exit(0)

}

func createNetwork(engine sim.Engine, test *acceptance.Test, config pcie.Config) {
	freq := 1.0 * sim.GHz
	var agents []*acceptance.Agent
	trafficManager := acceptance.NewTrafficManager()
	trafficManager.Degree = config.Degree
	trafficManager.Mesh = true
	for i := 0; i < 9; i++ {
		agent := acceptance.NewAgent(
			engine, freq, fmt.Sprintf("Agent%d", i),
			5, test, config.Ratio, &trafficManager.StopSignal)
		agent.TickLater(0)
		agents = append(agents, agent)
		agent.TrafficManager = trafficManager
		trafficManager.AddAgent(agent)
	}

	pcieConnector := pcie.NewConnector()
	pcieConnector = pcieConnector.
		WithEngine(engine).
		WithNetworkName("PCIe").
		WithVersion3().
		WithX16().
		WithDegree(config.Degree).
		WithNumVC(config.NumVC).
		WithSizeVC(config.SizeVC).
		WithConfig(config)
	pcieConnector.CreateNetwork()

	rootComplexID := pcieConnector.CreateRootComplex(agents[0].Ports)
	switch1ID := pcieConnector.AddSwitch(rootComplexID)
	for i := 1; i < 5; i++ {
		pcieConnector.PlugInDevice(switch1ID, agents[i].Ports)
	}

	switch2ID := pcieConnector.AddSwitch(rootComplexID)
	for i := 5; i < 9; i++ {
		pcieConnector.PlugInDevice(switch2ID, agents[i].Ports)
	}

	test.RegisterAgent(agents[1])
	test.RegisterAgent(agents[8])
}
