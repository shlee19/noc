package acceptance

import (
	"fmt"
	"math"
	"math/rand"
	"strings"

	"gitlab.com/akita/akita/v2/sim"
)

// TrafficManager occurs the network traffic
type TrafficManager struct {
	agents            []*Agent
	agentToNum        map[*Agent]int
	receivedMsgQueue  []sim.Msg
	totalReceivedMsgs []sim.Msg
	Degree            int

	Mesh      bool
	Flattened bool

	interval        float64
	lastCheckTime   float64
	previousLatency float64
	StopSignal      bool
	receivedTime    []float64

	msgs []sim.Msg
}

// NewTrafficManager make new traffic Manager
func NewTrafficManager() *TrafficManager {
	t := &TrafficManager{}
	t.agentToNum = make(map[*Agent]int)
	t.lastCheckTime = 0
	t.interval = float64(1000 / 1e9)
	t.previousLatency = 1 / 1e9
	t.StopSignal = false
	t.Mesh = false
	t.Flattened = false

	return t
}

// AddAgent the add the agents on TrafficManager
func (t *TrafficManager) AddAgent(agent *Agent) {
	t.agents = append(t.agents, agent)
	t.agentToNum[agent] = len(t.agents) - 1
}

// UniformRandom make the traffic of src to dest.
func (t *TrafficManager) UniformRandom(selfAgent *Agent) {
	srcAgentID := t.agentToNum[selfAgent]
	srcAgent := t.agents[srcAgentID]
	srcPortID := rand.Intn(len(srcAgent.Ports))
	srcPort := srcAgent.Ports[srcPortID]

	dstAgentID := rand.Intn(len(t.agents)-1) + 1
	for dstAgentID == srcAgentID && dstAgentID == 0 {
		dstAgentID = rand.Intn(len(t.agents)-1) + 1
	}

	dstAgent := t.agents[dstAgentID]
	dstPortID := rand.Intn(len(dstAgent.Ports))
	dstPort := dstAgent.Ports[dstPortID]

	msg := &trafficMsg{}
	msg.Meta().ID = sim.GetIDGenerator().Generate()
	msg.Src = srcPort
	msg.Dst = dstPort
	msg.TrafficBytes = 64
	srcAgent.MsgsToSend = append(srcAgent.MsgsToSend, msg)

	t.registerMsg(msg)
}

// StopCheck check the if network could be stall.
func (t *TrafficManager) StopCheck(selfAgent *Agent, msg sim.Msg) {
	t.receivedMsgQueue = append(t.receivedMsgQueue, msg)
	t.totalReceivedMsgs = append(t.totalReceivedMsgs, msg)

	var sumLatency float64
	numMsg := 0
	latestTime := float64(0.0)
	if float64(msg.Meta().RecvTime) > t.lastCheckTime {
		for _, recvMsg := range t.receivedMsgQueue {
			tempLatency := float64(recvMsg.Meta().RecvTime) - float64(recvMsg.Meta().SendTime)
			sumLatency = sumLatency + tempLatency
			t.receivedMsgQueue = t.receivedMsgQueue[1:]
			numMsg++
			if latestTime < float64(recvMsg.Meta().RecvTime) {
				latestTime = float64(recvMsg.Meta().RecvTime)
			}
		}
		tempLatency := sumLatency / float64(numMsg)
		t.lastCheckTime = t.lastCheckTime + t.interval
		if 0.90 < t.previousLatency/tempLatency &&
			t.lastCheckTime > float64(3000/1e9) {
			tempLatency = (tempLatency + t.previousLatency) / 2
			fmt.Printf("Latency : %0.2f \n", tempLatency*1e9)
			t.StopSignal = true
			t.MustHaveReceivedAllMsgs(latestTime)
		} else {
			t.previousLatency = tempLatency
			t.StopSignal = false
		}
	}
}

// averageHopCount caculate the hop count.
func (t *TrafficManager) averageHopCount(msg []sim.Msg) float64 {
	msgNum := float64(len(msg))
	sumHop := 0.0
	fmt.Printf("msg number : %d \n", len(msg))
	for _, sampleMsg := range msg {
		src := t.srcAgent(sampleMsg)
		dst := t.destAgent(sampleMsg)
		hop := t.calculationHopMesh(src, dst)
		sumHop = sumHop + math.Abs(hop)
	}
	return sumHop / msgNum
}

func (t *TrafficManager) destAgent(msgDst sim.Msg) int {
	for i := len(t.agents) + 1; i >= 0; i-- {
		if strings.Contains(msgDst.Meta().Dst.Name(), fmt.Sprintf("Agent%d", i)) {
			return i
		}
	}
	return -1
}

func (t *TrafficManager) srcAgent(msgSrc sim.Msg) int {
	for i := len(t.agents) + 1; i >= 0; i-- {
		if strings.Contains(msgSrc.Meta().Src.Name(), fmt.Sprintf("Agent%d", i)) {
			return i
		}
	}
	return -1
}

func (t *TrafficManager) calculationHopMesh(src int, dst int) float64 {
	quotientSrc := int64(src / t.Degree)
	remainderSrc := src % t.Degree
	quotientDst := int64(dst / t.Degree)
	remainderDst := dst % t.Degree
	x := math.Abs(float64(quotientSrc - quotientDst))
	y := math.Abs(float64(remainderSrc - remainderDst))
	return x + y
}

func (t *TrafficManager) registerMsg(msg sim.Msg) {
	t.msgs = append(t.msgs, msg)
}

func (t *TrafficManager) MustHaveReceivedAllMsgs(latestTime float64) {
	num := 0
	for _, msg := range t.totalReceivedMsgs {
		if float64(msg.Meta().RecvTime) <= latestTime {
			num++
		}
	}
	//if len(t.msgs) == len(t.receivedMsgQueue) {
	if num == len(t.totalReceivedMsgs) {
		fmt.Printf("Success!\n")
		return
	} else {
		fmt.Printf("Number of sent Msgs : %d \n", num)
		fmt.Printf("Number of recv Msgs : %d \n", len(t.totalReceivedMsgs))
		panic("some messages are droppend")
	}
}
